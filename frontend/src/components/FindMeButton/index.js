import React, { Component } from 'react';
import './index.css';

import LocationIcon from 'react-icons/lib/md/my-location';

class FindMeButton extends Component {
    render() {
        return (
            <div className='find-me-button'>
                <div id="btn-find-me" onClick={ this.props.findMe } className="button"><LocationIcon/></div>
            </div>
        );
    }
}

export default FindMeButton;
