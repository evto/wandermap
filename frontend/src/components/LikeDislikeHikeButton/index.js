import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Tooltip from "material-ui/Tooltip";
import IconButton from 'material-ui/IconButton';
import FavoriteUncheckedIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteCheckedIcon from '@material-ui/icons/Favorite';
import {likeHike, dislikeHike, fillLikeIcon, unfillLikeIcon} from "../../store/actions/hikeActions";


class LikeDislikeHikeButton extends Component {
  constructor(props) {
     super(props);

     this.state = {
         liked: this.props.hike.in_favorites,
         tooltipOpen: false,
     }
  }

  handleTooltipClose = () => {
    if (!this.props.isLoggedIn) {
        this.setState({ tooltipOpen: false });
    }
  };

  handleTooltipOpen = () => {
    if (!this.props.isLoggedIn) {
        this.setState({tooltipOpen: true});
    }
  };


  handleLike = () => {
      console.log('in like hike, hike id is ', this.props.hike.id);
      if (this.props.isLoggedIn) {
          this.props.dispatch(likeHike(this.props.hike.id));
          this.props.dispatch(fillLikeIcon(this.props.hike.id));
          this.setState({liked: true});
      }
  };

  handleDislike = () => {
      console.log('in dislike hike, hike id is ', this.props.hike.id);
      if (this.props.isLoggedIn) {
          this.props.dispatch(dislikeHike(this.props.hike.id));
          this.props.dispatch(unfillLikeIcon(this.props.hike.id));
          this.setState({liked: false});
      }
  };

  render() {
    // console.log('LikeDislikeHikeButton props', this.props);
    return (
      <div className='like-dislike-hike-button'>
        <Tooltip
        enterDelay={100}
        id="tooltip-controlled"
        leaveDelay={200}
        onClose={this.handleTooltipClose}
        onOpen={this.handleTooltipOpen}
        open={this.state.tooltipOpen}
        placement="bottom"
        title="Log in to like"
        >
        <IconButton size="small" color="secondary">
          { this.state.liked
              ? <FavoriteCheckedIcon onClick={this.handleDislike}/>
              : <FavoriteUncheckedIcon onClick={this.handleLike}/> }
        </IconButton>
        </Tooltip>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      isLoggedIn : state.currentUser.access
  }
};

export default connect(mapStateToProps)(LikeDislikeHikeButton);
