import React, { Component } from 'react';
import { connect } from "react-redux";
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import DirectionsIcon from '@material-ui/icons/Directions';
import {addTarget} from "../../store/actions/mapActions";


const styles = {
    root: {
      position: 'absolute',
      top: 295,
      right: 45,
    },
    addButton: {
      fontSize: 10,
      marginLeft: 8,
      marginTop: 3,
    },
    directionsButton: {
      fontSize: 64
    }
};

class DirectionsButton extends Component {

  addTarget = () => {
    console.log('Adding target to current targets', this.props.target);
    this.props.dispatch(addTarget(this.props.target));
  };

  render() {
    const { classes } = this.props;
    console.log('Add target button props: ', this.props);
    console.log('Add target button state: ', this.state);
    return (
      <div style={styles.root}>
          <IconButton onClick={this.addTarget} color="primary" >
            <DirectionsIcon className={classes.directionsButton}/>
          </IconButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  targets: state.map.targets,
});

export default connect(mapStateToProps)(withStyles(styles)(DirectionsButton));
