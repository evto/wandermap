import React, { Component } from 'react';
import { connect } from "react-redux";
import { withStyles } from 'material-ui/styles';
import moment from 'moment';
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import List, { ListItem } from 'material-ui/List';
import CardContent from 'material-ui/Card/CardContent';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';
// import ExpansionPanel from 'material-ui/ExpansionPanel';
// import ExpansionPanelSummary from 'material-ui/ExpansionPanel/ExpansionPanelSummary';
// import ExpansionPanelDetails from 'material-ui/ExpansionPanel/ExpansionPanelDetails';
// import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// import HikeStarRating from '../HikeStarRating';

import NewReviewForm from '../NewReviewForm';
import DefaultAvatar from '../../images/default_user_image.png';
import PercentageStarRating from "../PercentageStarRating";
import UsefulReviewButton from '../UsefulReviewButton';


const styles = {
  title: {
    color: '#4A4A4A',
    marginLeft: 30,
      display: 'inline-block'
  },
  avatar: {
    margin: 10,
  },
  button: {
    display: 'inline-block'
  },
  updateDeleteButton: {
    margin: '20px 20px 0 0'
  },
  usefulButton: {
      display: 'inline-block'
  },
  usefulQuestion: {
      color: '#4A4A4A',
      marginRight: 5,
      marginTop: 5
  }
};

class HikeReviews extends Component {

    render() {
      console.log('Hike review props', this.props);
      if (this.props.reviews === undefined) {
        return <Paper><CircularProgress /></Paper>;
      }
      return (
        <div className='hike-reviews-container'>
          <div style={styles.title}>
            <h4>REVIEWS</h4>
              {/*{this.props.}*/}
            <NewReviewForm hikeId={this.props.hikeId}/>
          </div>
          <List>
            {this.props.reviews.map((review, index) => (
              <div className='hike-review' key={index}>
                <ListItem>
                  <CardContent>
                    <Avatar alt="user avatar" src={review.user.profile.image || DefaultAvatar} style={styles.avatar} />
                    <Typography variant="subheading" component="h2">
                      {`${review.user.first_name} ${review.user.last_name}`}
                    </Typography>
                    {/*<HikeStarRating rating={review.rating}/>*/}
                    <PercentageStarRating rating={review.rating}/><br/>
                    <Typography variant='caption' color="textSecondary">
                      {moment(review.modified).format('MMMM Do YYYY, h:mm a')}
                    </Typography><br/>
                    <Typography component="p">
                      {review.content}
                    </Typography><br/>
                      {this.props.currentUser && review.user.id === this.props.currentUser.id
                          ? [<Button variant="raised" size="small" color="primary" style={styles.updateDeleteButton} key={'update'}>Update</Button>,
                             <Button variant="raised" size="small" color="secondary" style={styles.updateDeleteButton} key={'delete'}>Delete</Button>]
                          : ''}
                    <div className='useful-or-not-container'>
                      <span style={styles.usefulQuestion}>Useful?&#9;</span>
                      <div style={styles.usefulButton}>
                        <UsefulReviewButton review={review}/>
                      </div>
                    </div>
                </CardContent>
                </ListItem>
                {/*<ExpansionPanel>*/}
                  {/*<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>*/}
                    {/*<Typography variant='caption' color="secondary">View all comments</Typography>*/}
                  {/*</ExpansionPanelSummary>*/}
                  {/*<ExpansionPanelDetails>*/}
                    {/*<Typography>*/}
                      {/*Lorem ipsum dolor sit amet, consectetur adipiscing elit.*/}
                    {/*</Typography>*/}
                  {/*</ExpansionPanelDetails>*/}
                {/*</ExpansionPanel><br/>*/}
                <Divider/>
              </div>
              ))}
          </List>
        </div>
      );
    }
}

const mapStateToProps = (state) => {
    console.log('state in hike reviews', state)
    return {currentUser: state.currentUser.userData}
};

export default connect(mapStateToProps)(withStyles(styles)(HikeReviews));
