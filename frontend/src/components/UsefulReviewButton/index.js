import React, { Component } from 'react';
import { connect } from "react-redux";
import { withStyles } from 'material-ui/styles';
// import PropTypes from 'prop-types';
import Tooltip from "material-ui/Tooltip";
import IconButton from 'material-ui/IconButton';
import UsefulIcon from '@material-ui/icons/ThumbUp';
import NotUsefulIcon from '@material-ui/icons/ThumbDown';
import Typography from 'material-ui/Typography';
import {dislikeReview, likeReview, undislikeReview, unlikeReview} from "../../store/actions/reviewActions";

const styles = {
  useful: {
      display: 'inline-block'
  },
  count: {
      color: '#FFFFFF'
  },
  defaultIconButton: {
      color: '#A0A0A0',
  },
  filledIconButton: {
      color: '#414141'
  }
};

class UsefulReviewButton extends Component {
  constructor(props) {
     super(props);

     this.state = {
         useful: !this.props.review.liked && !this.props.review.disliked ? undefined : this.props.review.liked,
         usefulTooltipOpen: false,
         notUsefulTooltipOpen: false,
         likeCount: this.props.review.like_count,
         dislikeCount: this.props.review.dislike_count
     }
  }

  handleTooltipClose = (tooltipFor) => {
    if (!this.props.isLoggedIn) {
        if (tooltipFor === 'useful') {
            this.setState({usefulTooltipOpen: false});
        } else if (tooltipFor === 'notUseful') {
            this.setState({notUsefulTooltipOpen: false});
        }
    }
  };

  handleTooltipOpen = (tooltipFor) => {
    if (!this.props.isLoggedIn) {
        if (tooltipFor === 'useful') {
            this.setState({usefulTooltipOpen: true});
        } else if (tooltipFor === 'notUseful') {
            this.setState({notUsefulTooltipOpen: true});
        }
    }
  };

  handleUseful = () => {
      console.log('Useful button clicked');
      if (this.props.isLoggedIn) {
          if (this.state.useful === undefined || !this.state.useful) {
          const likes = this.state.likeCount + 1;
          if (!this.state.useful && this.state.dislikeCount > 0) {
              const dislikes = this.state.dislikeCount - 1;
              this.setState({dislikeCount: dislikes});
          }
          this.setState({
              useful: true,
              likeCount: likes,
          });
          this.props.dispatch(likeReview(this.props.review.id));
      } else if (this.state.useful) {
          if (this.state.likeCount > 0) {
              const likes = this.state.likeCount - 1;
              this.setState({likeCount: likes})
          }
          this.setState({useful: undefined});
          this.props.dispatch(unlikeReview(this.props.review.id));
        }
      }
  };

  handleNotUseful = () => {
      console.log('Not Useful button clicked');
      if (this.props.isLoggedIn) {
          if (this.state.useful === undefined || this.state.useful) {
              const dislikes = this.state.dislikeCount + 1;
              if (this.state.useful && this.state.likeCount > 0) {
                  const likes = this.state.likeCount - 1;
                  this.setState({likeCount: likes});
              }
              this.setState({
                  useful: false,
                  dislikeCount: dislikes,
              });
              this.props.dispatch(dislikeReview(this.props.review.id));
          } else if (!this.state.useful) {
              if (this.state.dislikeCount > 0) {
                  const dislikes = this.state.dislikeCount - 1;
                  this.setState({dislikeCount: dislikes})
              }
              this.setState({useful: undefined});
              this.props.dispatch(undislikeReview(this.props.review.id));
          }
      }
  };

  render() {
    // console.log('UsefulNotUseful review props', this.props);
    console.log('Useful review button state', this.state);
    return (
      <div className='useful-review-button'>
        <div style={styles.useful}>
          <Tooltip
            enterDelay={100}
            id="tooltip-controlled"
            leaveDelay={200}
            onClose={() => this.handleTooltipClose('useful')}
            onOpen={() => this.handleTooltipOpen('useful')}
            open={this.state.usefulTooltipOpen}
            placement="bottom"
            title="Log in to mark"
            >
            <IconButton
                size="small"
                color='default'
                onClick={this.handleUseful}
                style={this.state.useful ? styles.filledIconButton : styles.defaultIconButton}>
              <UsefulIcon/>
            </IconButton>
          </Tooltip>
        </div>
        <div style={styles.useful}>
          <Typography variant="body1">
              {this.state.likeCount}
          </Typography>
        </div>
          <div style={styles.useful}>
          <Tooltip
            enterDelay={100}
            id="tooltip-controlled"
            leaveDelay={200}
            onClose={() => this.handleTooltipClose('notUseful')}
            onOpen={() => this.handleTooltipOpen('notUseful')}
            open={this.state.notUsefulTooltipOpen}
            placement="bottom"
            title="Log in to mark"
          >
            <IconButton
                size="small"
                color='default'
                onClick={this.handleNotUseful}
                style={this.state.useful === false ? styles.filledIconButton : styles.defaultIconButton}>
              <NotUsefulIcon/>
            </IconButton>
          </Tooltip>
        </div>
        <div style={styles.useful}>
          <Typography variant="body1">
              {this.state.dislikeCount}
          </Typography>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      isLoggedIn : state.currentUser.access
  }
};

export default connect(mapStateToProps)(withStyles(styles)(UsefulReviewButton));
