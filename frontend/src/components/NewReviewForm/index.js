import React, { Component } from 'react';
import { connect } from "react-redux";
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import Dialog, {DialogActions, DialogContent, DialogContentText, DialogTitle} from 'material-ui/Dialog';
import InputAdornment from 'material-ui/Input/InputAdornment';
import StarIcon from '@material-ui/icons/StarBorder';
import ClearIcon from '@material-ui/icons/Clear';
import {submitReview} from "../../store/actions/hikeActions";

const styles = {
  dialog: {
    minWidth: 350,
    maxWidth: 550,
  },
};

class NewReview extends Component {

  constructor(props) {
    super(props);

    this.state = {
        newReviewOpen: false,
        content: '',
        rating: 4,

    }
  }

  handleClickOpen = () => {
    this.setState({ newReviewOpen: true });
  };

  handleClose = () => {
    this.setState({ newReviewOpen: false });
  };

  handleSubmit = () => {
    this.setState({ newReviewOpen: false })  ;
    this.props.dispatch(submitReview(this.props.hikeId, {content: this.state.content, rating: this.state.rating}));
  };

  changeReviewText = (e) => {
      const newText = e.currentTarget.value;
      this.setState({ content: newText });
    };

  clearReviewText = () => {
      this.setState({content: ''});
  };

  render() {
    console.log('New review state', this.state);
    return (
      <div className='new-review-container'>
        <Button onClick={this.handleClickOpen} variant="raised" color="secondary">Write a review</Button>
        <Dialog
          open={this.state.newReviewOpen}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">New Review</DialogTitle>
          <DialogContent style={styles.dialog}>
            <DialogContentText>
              <StarIcon/><StarIcon/><StarIcon/><StarIcon/><StarIcon/>
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              multiline
              rowsMax="4"
              fullWidth
              value = { this.state.content }
              onChange={ this.changeReviewText }
              InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                        <IconButton onClick={this.clearReviewText}><ClearIcon /></IconButton>
                    </InputAdornment>
                  ),
                }}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default connect()(NewReview);
