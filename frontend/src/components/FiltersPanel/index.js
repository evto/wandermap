import React from "react";
import FilterPanel from '../../components/FilterPanel';
import { selectors } from "../../store/constants";


const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

const FiltersPanel = () => (
  <div style={styles.root}>
    {selectors.map(
      (selector, index) =>
        <FilterPanel key={index} selector={selector}/>)
    }
  </div>
);

export default FiltersPanel;
