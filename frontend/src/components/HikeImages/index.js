import React, { Component } from 'react';
import './index.css';
import { withStyles } from 'material-ui/styles';
import image1 from '../../images/image1.jpg';
import image2 from '../../images/image2.jpg';
import image3 from '../../images/image3.jpg';
import image4 from '../../images/image4.jpg';
import image5 from '../../images/image5.jpg';
//import tileData from './tileData';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const styles = {
  root: {
    padding: 15,
    maxWidth: 675
  },
  gridList: {
    width: 500,
    height: 450,
  },
  slider: {

  },
  singleImage: {
    height: 450
  }

};

const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

// todo: get from backend
const tileData = [
    {
      img: image1,
      title: 'Image',
      author: 'author',
      cols: 2,
    },
    {
      img: image2,
      title: 'Image',
      author: 'author',
      cols: 1,
    },
    {
      img: image3,
      title: 'Image',
      author: 'author',
      cols: 1,
    },
    {
      img: image4,
      title: 'Image',
      author: 'author',
      cols: 2,
    },
    {
      img: image5,
      title: 'Image',
      author: 'author',
      cols: 2,
    },
  ];


class HikeImages extends Component {
    render() {
      return (
        <div className='hike-images-container' style={styles.root}>
          <Slider {...settings}>
            {tileData.map((tile, index) => (
              <div key={index}><img src={tile.img} alt='hike' style={styles.singleImage}/></div>
            ))}
      </Slider>
        </div>
      );
    }
}

export default withStyles(styles)(HikeImages);
