import React, { Component } from 'react';
import { connect } from "react-redux";
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemText, ListItemSecondaryAction, ListSubheader } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import LocationIcon from '@material-ui/icons/LocationOn';
import InfoIcon from '@material-ui/icons/Info';
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import {removeSource} from "../../store/actions/mapActions";


const styles = {
  subheader: {
    backgroundColor: '#ededed',
    fontWeight: 'bold'
  }
};

class MarkersList extends Component {
    constructor(props) {
      super(props);

      this.state = {
        addressBook: {},
      }

    }

    removeSource = (id) => {
      console.log('removing source from Redux state: ', id);
      this.props.dispatch(removeSource(id));
    };

    componentDidMount() {
        console.log('in Markers list component did mount');
    };

    render() {
      const { classes } = this.props;
      console.log('Markers List props', this.props);
      if (this.props.sources === undefined) {
        return <Paper><CircularProgress /></Paper>;
      }
      return (
        <div className='markers-list-container'>
          <List subheader={<ListSubheader className={classes.subheader}>{this.props.markersType} markers</ListSubheader>}>
            {this.props.sources.length === 0
              ? (<div>
                <ListItem>
                  <IconButton disabled color="primary" aria-label="Marker">
                    <InfoIcon />
                  </IconButton>
                  <ListItemText primary="No source markers defined"/>
                </ListItem></div>)
              : (<div>{this.props.sources.map((source, index) => (
                <div className='source' key={index}>
                  <ListItem>
                    <IconButton color="secondary" aria-label="Marker">
                      <LocationIcon />
                    </IconButton>
                    <ListItemText
                        primary={this.props.addressBook[`${source.latlng[0]},${source.latlng[1]}`]
                                 ? this.props.addressBook[`${source.latlng[0]},${source.latlng[1]}`]
                                 : ''}
                        secondary={`${source.latlng[0].toFixed(6)}, ${source.latlng[1].toFixed(6)}`}/>
                    <ListItemSecondaryAction>
                      <IconButton aria-label="Close" onClick={() => this.removeSource(index)}>
                        <CloseIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                </div>
                ))}</div>) }
          </List>
        </div>
      );
    }
}

const mapStateToProps = (state) => ({
  sources: state.map.sources,
  addressBook: state.map.addressBook
});

export default connect(mapStateToProps)(withStyles(styles)(MarkersList));
