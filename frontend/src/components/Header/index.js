import React, { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import UserIcon from '@material-ui/icons/AccountCircle';
import {login, logout} from "../../store/actions/currentUserActions";
import Logo from '../Logo';


const styles = {
  root: {
    flexGrow: 1,
      background: '#176d59',
      color: 'white',
    },
  flex: {
    flex: 1,
    cursor: 'pointer',
  },
  logoButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class Header extends Component {

    goToLogin = () => {
        this.props.dispatch(login(this.state));
        // if (this.props.isLoggedIn) {
            this.props.history.push('/login');
        // }
    };

    goToSignup = () => {
        this.props.history.push('/signup');
    };

    goToHome = () => {
        this.props.history.push('/');
        console.log('go to home');
    };

    goToSearch = () => {
        this.props.history.push('/filter');
    };

    goToTopHikes = () => {
        this.props.history.push('/top');
    };

    goToMyProfile = () => {
        this.props.history.push('/me');
    };

    logout = () => {
      this.props.dispatch(logout());
      this.props.history.push('/login');
    };

    render() {
        // console.log('Header props', this.props);
        return (
          <div className='Header'>
            <AppBar position="static" style={styles.root}>
              <Toolbar>
                <IconButton onClick={this.goToHome} style={styles.logoButton}>
                  <Logo />
                </IconButton>
                <Typography variant="button" color="inherit" style={styles.flex} onClick={this.goToHome}>
                  Wandermap
                </Typography>
                  <Button color="inherit" onClick={ this.goToSearch }>Search</Button>
                  <Button color="inherit" onClick={ this.goToTopHikes }>Top hikes</Button>
                  {this.props.isLoggedIn ? (
                      [<Button color="inherit" onClick={ this.logout } key='logout'>Log out</Button>,
                       <Button color="inherit" onClick={ this.goToMyProfile } key='myProfile'><UserIcon/></Button> ]
                    ) : ([<Button color="inherit" onClick={ this.goToLogin } key='login'>Log in</Button>,
                    <Button color="inherit" onClick={ this.goToSignup } key='signup'>Sign up</Button>]
                  )}

              </Toolbar>
            </AppBar>
          </div>
        )};
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.currentUser.access,
    }
};

export default connect(mapStateToProps)(withRouter(Header));
