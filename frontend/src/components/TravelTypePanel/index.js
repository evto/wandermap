import React, { Component } from 'react';
import './index.css';
import { connect } from "react-redux";

import WalkIcon from 'react-icons/lib/md/directions-walk';
import BikeIcon from 'react-icons/lib/md/directions-bike';
import CarIcon from 'react-icons/lib/md/directions-car';
import TransitIcon from 'react-icons/lib/md/directions-transit';
import { changeTravelMode } from "../../store/actions/mapActions";
import { WALK, BIKE, CAR, TRANSIT } from "../../store/constants";

class TravelTypePanel extends Component {

    componentDidUpdate() {
        this.props.showPolygons();
    }

    render() {
        // console.log('TravelTypePanel props', this.props);
        return (
            <div className='travel-type-panel' id='selectionBar'>
                <div id="btn-walk"
                     onClick={ () => this.changeMode(WALK) }
                     className={`button ${ WALK === this.props.travelMode ? 'active' : ''}`}><WalkIcon/></div>
                <div id="btn-bike"
                     onClick={ () => this.changeMode(BIKE) }
                     className={`button ${ BIKE === this.props.travelMode ? 'active' : ''}`}><BikeIcon/></div>
                <div id="btn-car"
                     onClick={ () => this.changeMode(CAR) }
                     className={`button ${ CAR === this.props.travelMode ? 'active' : ''}`}><CarIcon/></div>
                <div id="btn-transit"
                     onClick={ () => this.changeMode(TRANSIT) }
                     className={`button ${ TRANSIT === this.props.travelMode ? 'active' : ''}`}><TransitIcon/></div>
            </div>
        );
    }

    changeMode = (mode) => {
        this.props.dispatch(changeTravelMode(mode));
    }
}

const mapStateToProps = (state) => ({
     travelMode: state.map.travelMode,
});

export default connect(mapStateToProps)(TravelTypePanel);

