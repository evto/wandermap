import React, { Component } from 'react';
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import GridList, { GridListTile } from 'material-ui/GridList';
import HikeCard from "../HikeCard";

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    color: '#4A4A4A'
  },
  gridList: {
    width: '75%',
    height: '100vh',
    marginTop: 10
  },
};

class HikesCards extends Component {
  render() {
    console.log('hikes cards props', this.props);
    if (!this.props.hikes) {
      return <Paper><CircularProgress /></Paper>;
    }

    return (
      <div className='hikes-cards-container'>
          <div style={styles.root}>
              <GridList cellHeight={330} cols={3} style={styles.gridList}>
            { this.props.hikes.map((hike, index) => {
              return (
                <GridListTile cols={1} key={index}>
                  <HikeCard hike={hike}/>
                </GridListTile>
              );
          }) }
          </GridList>
          </div>
      </div>
    );
  }
}

export default HikesCards;
