import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import moment from 'moment';
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import List, { ListItem } from 'material-ui/List';
import CardContent from 'material-ui/Card/CardContent';

import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';

const styles = {
  title: {
    color: '#4A4A4A',
    marginLeft: 30
  }
};

class UserComments extends Component {

    render() {
      console.log('User comments props', this.props);
      if (this.props.comments === undefined) {
        return <Paper><CircularProgress /></Paper>;
      }
      return (
        <div className='user-comments-container'>
          <h4 style={styles.title}>COMMENTS</h4>
          <List>
        {this.props.comments.map((comment, index) => (
              <div className='user-comment' key={index}>
                <ListItem>
                  <CardContent>
                    <Typography variant="subheading" component="h2">
                      {`Review #${comment.review.id}`}
                    </Typography>
                    <Typography variant='caption' color="textSecondary">
                      {moment(comment.modified).format('MMMM Do YYYY, h:mm a')}
                    </Typography><br/>
                    <Typography component="p">
                      {comment.content}
                    </Typography>
                </CardContent>
                </ListItem>
                  <Divider/>
                  </div>
              ))}
          </List>
        </div>
      );
    }
}


export default withStyles(styles)(UserComments);
