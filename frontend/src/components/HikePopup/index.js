import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import TimeIcon from 'react-icons/lib/md/access-time';
import TerrainIcon from 'react-icons/lib/md/terrain';
import StarIcon from 'react-icons/lib/md/grade';
import WalkIcon from 'react-icons/lib/md/directions-walk';
import UpIcon from 'react-icons/lib/md/trending-up';
import DownIcon from 'react-icons/lib/md/trending-down';


const HikePopup = (route) => {

  return renderToStaticMarkup(
    <div className='hikePopup'>
      <div><h4><a href={ `/hikes/${route.id}` }>{ route.name }</a></h4></div>
      <div><StarIcon/> {route.rating.toFixed(1)} ({route.reviews.length} reviews)</div>
      <div><TimeIcon/>  { route.hiking_time } </div>
      <div><WalkIcon/> { route.length } km</div>
        {route.height_gain || route.height_loss
            ? <div><TerrainIcon/>
                {route.height_gain ? <span><UpIcon/>{route.height_gain}m </span> : ''}
                {route.height_loss ? <span><DownIcon/>{route.height_loss}m</span> : ''}
              </div>
            : ''}
    </div>
  );
};


export default HikePopup;
