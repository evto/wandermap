import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import 'css-star-rating/css/star-rating.css';


class HikeStarRating extends Component {
  render() {
    //console.log('Hike star rating props', this.props);
    return (
      <div className='hike-rating-container'>
          <div className={`rating medium star-icon value-${ Math.floor(this.props.rating) } color-ok label-left space-no`}>
              <div className="label-value">{this.props.rating}</div>
              <div className="star-container">
                  <div className="star">
                      <i className="star-empty"> </i>
                      <i className="star-half"> </i>
                      <i className="star-filled"> </i>
                  </div>
                  <div className="star">
                      <i className="star-empty"> </i>
                      <i className="star-half"> </i>
                      <i className="star-filled"> </i>
                  </div>
                  <div className="star">
                      <i className="star-empty"> </i>
                      <i className="star-half"> </i>
                      <i className="star-filled"> </i>
                  </div>
                  <div className="star">
                      <i className="star-empty"> </i>
                      <i className="star-half"> </i>
                      <i className="star-filled"> </i>
                  </div>
                  <div className="star">
                      <i className="star-empty"> </i>
                      <i className="star-half"> </i>
                      <i className="star-filled"> </i>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default HikeStarRating;
