import L from "leaflet";

import easyIconUrl from '../../images/easy.png';
import mediumIconUrl from '../../images/medium.png';
import hardIconUrl from '../../images/hard.png';

const HikeIcon = L.Icon.extend({
      options: {
        iconSize:     [50, 50],
        iconAnchor:   [5, 49],
        popupAnchor:  [25, -40],
      }
});

const easyIcon = new HikeIcon({iconUrl: easyIconUrl});
const mediumIcon = new HikeIcon({iconUrl: mediumIconUrl});
const hardIcon = new HikeIcon({iconUrl: hardIconUrl});

export const setIcon = (level) => {
  switch(level) {
    case 'EASY':
      return easyIcon;
    case 'MEDIUM':
      return mediumIcon;
    case 'HARD':
      return hardIcon;
    default:
      return easyIcon;
  }
};
