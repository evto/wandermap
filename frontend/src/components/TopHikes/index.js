import React, { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Header from '../Header';
import HikesCards from '../HikesCards';
import {fetchTopHikes} from "../../store/actions/hikeActions";

const styles = {
  title: {
    textAlign: 'center',
    height: 'auto',
    padding: 20,
    marginTop: 20,
    fontWeight: 'bold',
    color: '#4A4A4A'
  }
};

class TopHikes extends Component {

  componentDidMount() {
      console.log('Top hikes component did mount');
      this.props.dispatch(fetchTopHikes());
  }

  render() {
    console.log('top hikes props', this.props);
    if (!this.props.topHikes) {
      return <Paper><CircularProgress /></Paper>;
    }

    return (
      <div className='hikes-cards-container'>
          <Header/>
          <div style={styles.title}>
              <Typography variant="title" color="inherit">
                BEST RATED HIKING ROUTES
              </Typography>
          </div>
          <HikesCards hikes={this.props.topHikes}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    console.log('state in top hiikes', state);
    return {
        topHikes: state.hikes.top,
    }
};

export default connect(mapStateToProps)(withRouter(TopHikes));
