import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { connect } from "react-redux";
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import HikeDrawerContent from '../HikeDrawerContent';
import {hideHikeDrawer} from "../../store/actions/mapActions";

const styles = {
  drawerPaper: {
    minWidth: 400,
    width: '30vw',
  },
  menuButton: {
    position: 'absolute',
    top: 85,
    left: 25,
    zIndex: 10
  },
  closeButton: {
    float: 'right',
  }
};

class HikeDrawer extends Component {

  hideDrawer = () => {
    this.props.dispatch(hideHikeDrawer());
  };

  render() {
    const { classes } = this.props;
    console.log('Hike drawer props: ', this.props);
    return (
      <div className="hike-drawer-container">
        <Drawer
            variant={this.props.hike !== undefined ? 'permanent' : ''}
            open={this.props.hike !== undefined}
            classes={{paper: classes.drawerPaper}}
        >
          <div>
            <IconButton
              className={classes.closeButton}
              onClick={this.hideDrawer}
            >
            <CloseIcon/>
            </IconButton>
          </div>
          <HikeDrawerContent hike={this.props.hike}/>
        </Drawer>
      </div>
    );
  }
}

HikeDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
     hike: state.map.clickedHike
});


export default connect(mapStateToProps)(withStyles(styles)(HikeDrawer));
