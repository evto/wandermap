import React, { Component } from 'react';
import { connect } from "react-redux";
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import List, { ListItem, ListItemText} from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import {addAddress, addSource} from "../../store/actions/mapActions";


const styles = {
    addButton: {
       margin: '0 auto 30px 120px',
    },
    inputField: {
      textAlign: 'center',
      width: '80%',
      minWidth: 150,
      margin: 15,
    }
};

class SourceInputField extends Component {
  constructor(props) {
      super(props);

      this.state = {
          address: '',
          latlng: [],
      };
  }

  clearState = () => {
    this.setState({address: '', latlng: []})
  };

  handleChange = (address) => {
    this.setState({ address })
  };

  handleSelect = (address) => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
          console.log('Success', latLng);
          this.setState({latlng: [latLng.lat, latLng.lng]});
      })
      .catch(error => console.error('Error', error))
  };

  addSource = () => {
    console.log('Adding source to current sources', this.props.sources);
    this.props.dispatch(addAddress(`${this.state.latlng[0]},${this.state.latlng[1]}`));
    this.props.dispatch(addSource(this.state.latlng));
    this.clearState();
  };

  render() {
    const { classes } = this.props;
    // console.log('Source Input props: ', this.props);
    // console.log('Source Input state: ', this.state);
    return (
      <div>
        <Divider/>
        <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
          <div>
            <TextField
              {...getInputProps({
                label: 'Add source marker ...',
                className: classes.inputField,
              })}
            />
            <IconButton onClick={this.clearState}><CloseIcon/></IconButton>
            <List className="autocomplete-dropdown-container">
              {suggestions.map(suggestion => {
                console.log('Suggestion', suggestion);
                const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                const style = suggestion.active
                            ? { backgroundColor: '#9df9c5', cursor: 'pointer' }
                            : { backgroundColor: '#cfccff', cursor: 'pointer' };
                return (
                  <ListItem {...getSuggestionItemProps(suggestion, { className, style })}>
                    <ListItemText primary={suggestion.description}/>
                    <Divider/>
                  </ListItem>
                )
              })}
            </List>
            <Button variant="raised" color="primary" className={classes.addButton} onClick={this.addSource}>
              Add source
            </Button>
          </div>
        )}
      </PlacesAutocomplete>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  sources: state.map.sources,
  addressBook: state.map.addressBook
});

export default connect(mapStateToProps)(withStyles(styles)(SourceInputField));
