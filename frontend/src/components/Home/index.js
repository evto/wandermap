import React, { Component } from 'react';
import './index.css';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import r360 from "route360";
import L from "leaflet";
import '../../utils/weather/leaflet-openweathermap';
import '../../utils/weather/Permalink';
import '../../utils/weather/leaflet-openweathermap.css';
import '../../utils/weather/leaflet-languageselector';

import '../../utils/weather/map_i18n';

import {apikey, polygonColors, serviceUrl, owmKey} from "../../store/constants";
import Header from "../Header";
import Map from "../Map";
import TravelTypePanel from "../TravelTypePanel";
import RushHourCheckbox from  '../RushOurCheckbox';
// import TravelTimePanel from "../TravelTimePanel";
import TravelTimeLegend from "../TravelTimeLegend";
import FindMeButton from "../FindMeButton";
import HikePopup  from '../HikePopup';
import LeftDrawer  from '../LeftDrawer';
import HikeDrawer  from '../HikeDrawer';
import Hikes from "../../containers/Hikes";

import {
    addAddress, addSource,
    allowLocationAccess, clearTargets,
    setUserLocation, showHikeDrawer,
    updateSources
} from "../../store/actions/mapActions";
import {setIcon} from "../HikeMarker";

class Home extends Component {
    constructor(props) {
        super(props);
        this.map = {};
    }

    componentDidMount() {
        // console.log('in home component did mount');
        const output = document.getElementById('map');
        this.initMap(this.props.userLocation, this.props.mapZoom);

        const userLocation = JSON.parse(localStorage.getItem('userLocation'));
        console.log('user location from local storage', userLocation);
        if (!userLocation) {
            if (window.confirm('wandermap wants to know you location')) {
                console.log('user allowed to access his location');
                if (!navigator.geolocation) {
                    console.log('Geolocation is not supported by your browser');
                    output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
                    return;
                }
                this.findMe();
            } else {
                localStorage.setItem('userLocation', JSON.stringify(this.props.userLocation));
                this.props.dispatch(addAddress(`${this.props.userLocation[0]},${this.props.userLocation[1]}`));
                this.props.dispatch(addSource(this.props.userLocation));
                this.map.sourceMarker = L.marker(this.props.userLocation, { draggable: true }).addTo(this.map.markerGroup);
                this.map.polygonLayer = r360.leafletPolygonLayer().addTo(this.map);
                this.map.polygonLayer.setColors(polygonColors);
            }
        } else {
            // TODO: map last position should be saved and map should be loaded from the last position
            this.props.dispatch(addAddress(`${userLocation[0]},${userLocation[1]}`));
            this.props.dispatch(addSource(userLocation));
            this.map.setView(userLocation, this.props.mapZoom);
            this.map.sourceMarker = L.marker(userLocation, { draggable: true }).addTo(this.map.markerGroup);
            this.map.polygonLayer = r360.leafletPolygonLayer().addTo(this.map);
            this.map.polygonLayer.setColors(polygonColors);
        }
    }

    componentDidUpdate() {
      // if (this.map.polygonLayer) {
          this.map.markerGroup.clearLayers();
          this.map.sourceMarkers = [];
          this.map.targetMarkers = [];
          if (this.props.sources && this.props.sources.length > 0) {
              this.props.sources.forEach((source, index) => {
                  console.log('adding marker for source', source);
                  const sourceMarker = L.marker(source.latlng, {draggable: true}).addTo(this.map.markerGroup);
                  this.map.sourceMarkers.push(sourceMarker);
                  sourceMarker.on('dragend', (e) => {
                      console.log('drag end coords', e.target._latlng);
                      console.log('drag end index', index);
                      this.props.dispatch(updateSources([e.target._latlng.lat, e.target._latlng.lng], index));
                      this.props.dispatch(addAddress(`${e.target._latlng.lat},${e.target._latlng.lng}`));
                      this.showPolygons(true);
                  });
              });

          }
          if (this.props.targets && this.props.targets.length > 0) {
              this.props.targets.forEach((target) => {
                  const targetMarker = L.marker(target.latlng).setOpacity(0).addTo(this.map.targetGroup);
                  this.map.targetMarkers.push(targetMarker);
                  console.log('added target marker');
              })
          }
          this.showPolygons();
      // }
    }

    // componentWillUnmount() {
    //     console.log('In home component did unmount');
    //     this.saveMapPosition(this.map.getCenter(), this.map.getZoom());
    // }

    initMap(center, zoom) {
        const renderer = L.canvas();
        this.map = L.map('map', { zoomControl: false, renderer }).setView(center, zoom);
        // L.control.zoom({ position:'bottomleft' }).addTo(this.map);
        this.map.markerGroup = L.layerGroup().addTo(this.map);
        this.map.targetGroup = L.layerGroup().addTo(this.map);
        this.map.targetLayer = L.featureGroup().addTo(this.map);
        this.map.routeLayer = L.featureGroup().addTo(this.map);
        this.map.attributionControl.addAttribution("ÖPNV Daten © <a href='https://www.vbb.de/de/index.html' target='_blank'>VBB</a>");
        r360.basemap({ style: 'basic', apikey: apikey }).addTo(this.map);
        this.drawHikes(this.props.routes);


        this.map.weather = L.layerGroup().addTo(this.map);
        const clouds = L.OWM.clouds({opacity: 0.8, legendImagePath: '../../utils/weather/NT2.png', appId: owmKey});
        const precipitation = L.OWM.precipitation( {opacity: 0.5, appId: owmKey} );
        const precipitationcls = L.OWM.precipitationClassic({opacity: 0.5, appId: owmKey});
        let city = L.OWM.current({intervall: 15, imageLoadingUrl: '../../utils/weather/owmloading.gif', lang: 'en', minZoom: 5,
                 appId: owmKey});

        let baseMaps = {};

        let overlayMaps = {
            clouds,
            precipitation,
            precipitationcls,
            city
        };


	    let layerControl = L.control.layers(baseMaps, overlayMaps, {collapsed: false}).addTo(this.map);
	    this.map.addControl(new L.Control.Permalink({layers: layerControl, useAnchor: false, position: 'bottomright'}));

    }

    findMe = () => {
        const geoOptions = {
                maximumAge: 5 * 60 * 1000, // 1h
                timeout: 10 * 1000, // 10s
            };
        const geoSuccess = (position) => {
                const latitude = position.coords.latitude;
                const longitude = position.coords.longitude;

                this.props.dispatch(addAddress(`${latitude},${longitude}`));
                this.props.dispatch(addSource([latitude, longitude]));
                this.props.dispatch(setUserLocation([latitude, longitude]));
                localStorage.setItem('userLocation', JSON.stringify(this.props.userLocation));
                this.map.setView(this.props.userLocation, this.props.mapZoom);
                this.map.polygonLayer = r360.leafletPolygonLayer().addTo(this.map);
                this.map.polygonLayer.setColors(polygonColors);
        };
        const geoError = (error) => {
            console.log('Unable to retrieve your location. Error code: ' + error.code);
        };
        navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
        this.props.dispatch(allowLocationAccess());
    };


    drawHikes = (hikes) => {
        this.map.targetLayer.clearLayers();
        if (hikes) {
            // console.log('drawing hiking routes starting points');
            hikes.forEach((route) => {
                const circleMarker = L.marker(route.starting_point, {
                    color: "white",
                    fillColor: '#9213b2',
                    fillOpacity: 1.0,
                    stroke: true,
                    radius: 6,
                    icon: setIcon(route.technique),
                }).addTo(this.map.targetLayer).bindPopup(HikePopup(route));
                circleMarker.on('mouseover', () => circleMarker.openPopup());
                circleMarker.on('mouseout', () => circleMarker.closePopup());
                circleMarker.on('click', () => {
                    console.log(circleMarker._latlng);
                    this.props.dispatch(showHikeDrawer(route));
                    this.props.dispatch(clearTargets());
                });
            })
        }
    };



    showPolygons = () => {
        console.log("Showing polygons for travel mode ", this.props.travelMode);
        this.map.routeLayer.clearLayers();
        const travelOptions = r360.travelOptions();
        travelOptions.setServiceKey(apikey);
        travelOptions.setServiceUrl(serviceUrl);
        travelOptions.setIntersectionMode(this.props.intersectionMode);
        travelOptions.setRecommendations(-1);
        travelOptions.setRushHour(this.props.rushHour);
        if (this.map.sourceMarkers) {
            this.map.sourceMarkers.forEach(sourceMarker => {
             travelOptions.addSource(sourceMarker);
            });
        } else {
            console.log('No source markers found');
            //return;
        }
        travelOptions.setTravelTimes(this.props.travelTimes);
        travelOptions.setTravelType(this.props.travelMode);
        r360.PolygonService.getTravelTimePolygons(travelOptions, (polygons) => {
          this.map.polygonLayer.clearAndAddLayers(polygons, true);
        });
        if (this.map.targetMarkers && this.map.targetMarkers.length > 0) {
            // console.log('found targets');
            travelOptions.setTargets(this.map.targetMarkers);
            r360.RouteService.getRoutes(travelOptions, (routes) => {
                console.log('drawing ROUTES from sources to targtes');
                routes.forEach((route) => {
                    // console.log("Route: ", route);
                    r360.LeafletUtil.fadeIn(this.map.routeLayer, route, 1000, "travelDistance");
                });
            });
        }

    };

    render() {
        console.log('Home page props', this.props);
        console.log('Home page map', this.map);
        return (
            <div className='homepage'>
                <Header />
                <LeftDrawer/>
                <HikeDrawer/>
                <Map />
                <Hikes drawHikes={ this.drawHikes }/>
                <TravelTypePanel showPolygons={ this.showPolygons }/>
                <RushHourCheckbox showPolygons={ this.showPolygons }/>
                {/*<TravelTimePanel showPolygons={ this.showPolygons }/>*/}
                <TravelTimeLegend />
                <FindMeButton findMe={ this.findMe }/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userLocationAccess: state.map.userLocationAccess,
        userLocation: state.map.userLocation,
        mapCenter: state.map.userLocation,
        mapZoom: state.map.zoom,
        travelMode: state.map.travelMode,
        travelTime: state.map.travelTime,
        travelTimes: state.map.travelTimes,
        intersectionMode: state.map.intersectionMode,
        rushHour: state.map.rushHour,
        routes: state.hikes.routes,
        sources: state.map.sources,
        targets: state.map.targets,
    }
};

export default connect(mapStateToProps)(withRouter(Home));
