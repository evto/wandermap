import React, { Component } from 'react';
import { connect } from "react-redux";
// import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import Tooltip from "material-ui/Tooltip";
import BookmarkUncheckedIcon from '@material-ui/icons/BookmarkBorder';
import BookmarkCheckedIcon from '@material-ui/icons/Bookmark';
import {
    bookmarkHike,
    fillBookmarkIcon,
    unbookmarkHike, unfillBookmarkIcon,
} from "../../store/actions/hikeActions";


class BookmarkUnbookmarkHikeButton extends Component {
  constructor(props) {
     super(props);

     this.state = {
         bookmarked: this.props.hike.in_bookmarks,
         tooltipOpen: false,
     }
  }

  handleTooltipClose = () => {
    if (!this.props.isLoggedIn) {
        this.setState({ tooltipOpen: false });
    }
  };

  handleTooltipOpen = () => {
    if (!this.props.isLoggedIn) {
        this.setState({tooltipOpen: true});
    }
  };

  handleBookmark = () => {
      if (this.props.isLoggedIn) {
          this.props.dispatch(bookmarkHike(this.props.hike.id));
          this.props.dispatch(fillBookmarkIcon(this.props.hike.id));
          this.setState({bookmarked: true});
      }
  };

  handleUnbookmark = () => {
      if (this.props.isLoggedIn) {
          this.props.dispatch(unbookmarkHike(this.props.hike.id));
          this.props.dispatch(unfillBookmarkIcon(this.props.hike.id));
          this.setState({bookmarked: false});
      }
  };

  render() {
    // console.log('BookmarkUnbookmarkHikeButton props', this.props);
    return (
      <div className='bookmark-unbookmark-hike-button'>
        <Tooltip
        enterDelay={100}
        id="tooltip-controlled"
        leaveDelay={200}
        onClose={this.handleTooltipClose}
        onOpen={this.handleTooltipOpen}
        open={this.state.tooltipOpen}
        placement="bottom"
        title="Log in to bookmark"
        >
        <IconButton size="small" color="primary">
          { this.state.bookmarked
              ? <BookmarkCheckedIcon onClick={this.handleUnbookmark}/>
              : <BookmarkUncheckedIcon onClick={this.handleBookmark}/> }
        </IconButton>
        </Tooltip>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      isLoggedIn : state.currentUser.access
  }
};

export default connect(mapStateToProps)(BookmarkUnbookmarkHikeButton);
