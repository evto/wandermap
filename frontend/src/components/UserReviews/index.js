import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import moment from 'moment';
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import List, { ListItem } from 'material-ui/List';
import CardContent from 'material-ui/Card/CardContent';
import Button from 'material-ui/Button';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';
// import HikeStarRating from '../HikeStarRating';
import PercentageStarRating from "../PercentageStarRating";


const styles = {
  root: {
    width: '60%',
    minWidth: 480,
    margin: 'auto',
  },
  updateDeleteButton: {
    margin: '20px 20px 0 0'
  },
};

class UserReviews extends Component {

    render() {
      // console.log('User review props', this.props);
      if (this.props.reviews === undefined) {
        return <Paper><CircularProgress /></Paper>;
      }
      return (
        <div className='user-reviews-container' style={styles.root}>
          <List>
            {this.props.reviews.map((review, index) => (
              <div className='user-review' key={index}>
                <ListItem>
                  <CardContent>
                    <Typography variant="subheading" component="h2">
                      {review.hike.name}
                    </Typography>
                    <PercentageStarRating rating={review.rating}/><br/>
                    <Typography variant='caption' color="textSecondary">
                      {moment(review.modified).format('MMMM Do YYYY, h:mm a')}
                    </Typography><br/>
                    <Typography component="p">
                      {review.content}
                    </Typography>
                    <div>
                      <Button variant="raised" size="small" color="primary" style={styles.updateDeleteButton}>Update</Button>
                      <Button variant="raised" size="small" color="secondary" style={styles.updateDeleteButton}>Delete</Button>
                    </div>
                </CardContent>
                </ListItem>
                  <Divider/>
                  </div>
              ))}
          </List>
        </div>
      );
    }
}


export default withStyles(styles)(UserReviews);
