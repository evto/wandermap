import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import CardContent from 'material-ui/Card/CardContent';
import Typography from 'material-ui/Typography';
import Avatar from 'material-ui/Avatar';
// import EditIcon from '@material-ui/icons/Edit';
import Background from '../../images/default_user_background.png';
import defaultAvatar from '../../images/default_user_image.png';


const styles = {
  card: {
    display: 'flex',
    padding: 20,
    backgroundImage: `url(${Background})`,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    flexShrink: 0,
    flexBasis: 300
  },
  content: {
    flex: '1 0 auto',

  },
  avatar: {
    marginLeft: '15vw',
    width: 160,
    height: 160,
  },
  bioText: {
      color: 'white',
      textShadow: '1px 1px 1px #828282'
  }
};

class UserBio extends Component {

    render() {
      console.log('User bio props', this.props);
      if (!this.props.user) {
        return <Paper><CircularProgress /></Paper>;
      }
      return (
        <div className='user-bio-container' style={styles.card}>
          {/*<Card style={styles.card}>*/}
            <Avatar
                alt="avatar"
                src={this.props.user.profile.image ? this.props.user.profile.image : defaultAvatar}
                style={styles.avatar} />
            <div style={styles.details}>
              <CardContent style={styles.content}>
                <Typography style={styles.bioText} variant="display1">{ this.props.user.first_name } { this.props.user.last_name }</Typography>
                <Typography style={styles.bioText} variant="headline" color="textSecondary">{ this.props.user.profile.location }</Typography><br/>
                <Typography
                    style={styles.bioText}
                    variant="subheading"
                    color="textSecondary">{ `${this.props.user.reviews.length} review(s)` }
                </Typography>
                {/*<Typography*/}
                    {/*variant="subheading"*/}
                    {/*color="textSecondary">{ `${this.props.user.comments.length} comment(s)` }*/}
                {/*</Typography>*/}
              </CardContent>
            </div>
          {/*</Card>*/}
        </div>
      );
    }
}


export default withStyles(styles)(UserBio);
