import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import MarkersList from '../MarkersList';
import FiltersPanel from '../FiltersPanel';
import SourceInputField from '../SourceInputField';

const styles = {
  drawerPaper: {
    minWidth: 400,
    width: '30vw',
  },
  menuButton: {
    position: 'absolute',
    top: 85,
    left: 25,
    zIndex: 10
  },
  closeButton: {
    float: 'right',
  }
};

class LeftDrawer extends Component {
  constructor(props) {
      super(props);

      this.state = {
          left: false,
      };
  }

  toggleDrawer = (open) => () => {
    this.setState({
      left: open,
    });
  };

  render() {
    const { classes } = this.props;
    console.log('Left drawer props: ', this.props);
    return (
      <div className="left-drawer-container">
        <Button variant="fab" color="secondary" onClick={this.toggleDrawer(true)} className={classes.menuButton}>
          <MenuIcon/>
        </Button>
        <Drawer
            variant={this.state.left ? 'permanent' : ''}
            open={this.state.left}
            onClose={this.toggleDrawer(false)}
            classes={{paper: classes.drawerPaper}}
        >
          <div>
            <IconButton
              className={classes.closeButton}
              onClick={this.toggleDrawer(false)}
              onKeyDown={this.toggleDrawer(false)}>
            <CloseIcon/>
            </IconButton>
            <FiltersPanel/>
            <SourceInputField/>
            <MarkersList markersType={'Source'}/>
          </div>
        </Drawer>
      </div>
    );
  }
}

LeftDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LeftDrawer);
