import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { connect } from "react-redux";

import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import Input from 'material-ui/Input';
import InputLabel from 'material-ui/Input/InputLabel';
import MenuItem from 'material-ui/Menu/MenuItem';
import FormControl from 'material-ui/Form/FormControl';
import ListItemText from 'material-ui/List/ListItemText';
import Select from 'material-ui/Select';
import Checkbox from 'material-ui/Checkbox';
import {addFilter} from "../../store/actions/hikeActions";


const styles = {
  formControl: {
    margin: 10,
    minWidth: 150,
    maxWidth: 300,
  },
};

class FilterPanel extends Component {

  constructor(props){
    super(props);

    this.state = {
        selector: [], // multiple select
    };
  }

  handleChange = event => {
    this.setState({ selector: event.target.value });
    const newFilter = {};
    const key = this.props.selector.key;
    newFilter[key] = event.target.value;
    this.props.dispatch(addFilter(newFilter));
  };

  componentDidMount() {
    if (this.props.choices) {
      this.setState({selector: this.props.choices});
    }
  }

  render() {
    // console.log('Filter panel props', this.props);
    if (!this.props.selector) {
          return <Paper><CircularProgress /></Paper>;
        }
    return (
      <div className='filter-panel-container'>
        <FormControl style={styles.formControl}>
          <InputLabel htmlFor="select-multiple-checkbox">{this.props.selector.name}</InputLabel>
          <Select
            multiple
            value={this.state.selector}
            onChange={this.handleChange}
            input={<Input id="select-multiple-checkbox" />}
            renderValue={selected => selected.join(', ')}
          >
            {this.props.selector.choices.map(choice => (
              <MenuItem key={choice} value={choice}>
                <Checkbox checked={this.state.selector.indexOf(choice) > -1} />
                <ListItemText primary={choice} />
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return { choices: state.hikes.filters[ownProps.selector.key]}
};

export default connect(mapStateToProps)(withStyles(styles)(FilterPanel));
