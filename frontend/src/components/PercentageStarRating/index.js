import React, {Component} from "react";
import './index.scss';
import './stars.css';
import 'font-awesome/css/font-awesome.min.css';


class PercentageStarRating extends Component {
    render() {
      return (
        <div>
        <div className="rating-score">{this.props.rating.toFixed(1)}</div>
        <div className="ratings">
          <div className="empty-stars"> </div>
          <div className="full-stars" style={{width: `${20 * this.props.rating}%`}}> </div>
        </div>
        </div>
    );
    }
}

export default PercentageStarRating;
