import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
    import { connect } from "react-redux";
// import { withStyles } from 'material-ui/styles';

import Card from 'material-ui/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardContent from 'material-ui/Card/CardContent';
import CardMedia from 'material-ui/Card/CardMedia';
import Typography from 'material-ui/Typography';

import LikeDislikeHikeButton from "../LikeDislikeHikeButton";
import BookmarkUnbookmarkHikeButton from "../BookmarkUnbookmarkHikeButton";
import DirectionsButton from '../DirectionsButton';
import HikeReviews from '../HikeReviews';

import DefaultHikeImage from '../../images/default_hike_image.png';
import PercentageStarRating from "../PercentageStarRating";
import {clearTargets, hideHikeDrawer} from "../../store/actions/mapActions";

const styles = {
  card: {
    width: '100%'
  },
  media: {
    height: 0,
    cursor: 'pointer',
    paddingTop: '56.25%', // 16:9
  },
  link: {
    cursor: 'pointer',
  },
  directionsButton: {
    position: 'absolute',
    top: '-50px',
    left: 50,
  }
};


class HikeDrawerContent extends Component {

  handleClick = () => {
    this.props.history.push(`/hikes/${this.props.hike.id}`);
    this.props.dispatch(hideHikeDrawer());
  };

  render() {
    // console.log('SingleHikeCard props', this.props);
    return (
      <div className='hike-card-container'>
        <Card style={styles.card}>
        <CardMedia
          style={styles.media}
          image={this.props.hike.images.length > 0 ? this.props.hike.images[0].image : DefaultHikeImage}
          title="Sample hike image"
          onClick={this.handleClick}
        />
        <CardContent>
          <Typography gutterBottom component="h4" onClick={this.handleClick} style={styles.link}>
            { this.props.hike.name }
          </Typography>
          <PercentageStarRating rating={ this.props.hike.rating }/>
          <Typography component="p">
              { this.props.hike.reviews.length } reviews
          </Typography>

        </CardContent>
        <CardActions>
          <BookmarkUnbookmarkHikeButton hike={this.props.hike}/>
          <LikeDislikeHikeButton hike={this.props.hike}/>
          <DirectionsButton target={this.props.hike ? this.props.hike.starting_point : undefined} style={styles.directionsButton}/>
        </CardActions>
      </Card>
      <HikeReviews reviews={this.props.hike.reviews} hikeId={this.props.hike.id} />
      </div>
    );
  }
}

export default connect()(withRouter(HikeDrawerContent));
