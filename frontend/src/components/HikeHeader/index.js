import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
// import HikeStarRating from "../HikeStarRating";
import CardContent from 'material-ui/Card/CardContent';
import CardActions from 'material-ui/Card/CardActions';
import Typography from 'material-ui/Typography';
import PercentageStarRating from "../PercentageStarRating";
import LikeDislikeHikeButton from "../LikeDislikeHikeButton";
import BookmarkUnbookmarkHikeButton from '../BookmarkUnbookmarkHikeButton';

const styles = {
  root: {
    display: 'flex',
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: 10,
    paddingBottom: 10,
  },
};


class HikeHeader extends Component {
    render() {
      return (
        <div className='hike-header-container' style={styles.root}>
          <div>
          <CardContent >
            <Typography variant='title'>
              { this.props.hike.name }
            </Typography>
            <PercentageStarRating rating={this.props.hike.rating}/>
            <Typography component="p">
              { this.props.hike.reviews.length } reviews
            </Typography>
          </CardContent>
            </div>
            <div>
          <CardActions>
            <BookmarkUnbookmarkHikeButton hike={this.props.hike}/>
            <LikeDislikeHikeButton hike={this.props.hike}/>
          </CardActions>
          </div>
        </div>
      );
    }
}

export default withStyles(styles)(HikeHeader);
