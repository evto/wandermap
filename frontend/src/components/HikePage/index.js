import React, { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
// import AccessTimeIcon from '@material-ui/icons/AccessTime';
// import TerrainIcon from '@material-ui/icons/Terrain';
// import NavigationIcon from '@material-ui/icons/Navigation';

import Table from 'material-ui/Table';
import TableBody from 'material-ui/Table/TableBody';
import TableCell from 'material-ui/Table/TableCell';
import TableRow from 'material-ui/Table/TableRow';


import Header from '../Header';
import HikeImages from '../HikeImages';
import HikeHeader from '../HikeHeader';
import HikeReviews from '../HikeReviews';


const styles = {
  table: {
    maxWidth: 500,
  },
  hikeData: {
    width: '50%',
    float: 'left',
    boxSizing: 'border-box',
    padding: 20
  },
  reviews: {
    width: '50%',
    float: 'left',
    boxSizing: 'border-box',
    padding: 20

  }
};


class HikePage extends Component {

  render() {
    console.log('Hike page props', this.props);
    console.log('Hike page state', this.hikeId);
    if (!this.props.hike) {
      return <Paper><CircularProgress /></Paper>;
    }
    return (
      <div className='hike-page-container'>
        <Header />
        <div className='hike-data' style={styles.hikeData}>
          <HikeHeader hike={this.props.hike}/>
          <HikeImages/><br/>
          <Table style={styles.table}>
            <TableBody>
              <TableRow>
                  <TableCell component="th" scope="row">Route</TableCell>
                  <TableCell>{this.props.hike.route}</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Hiking time</TableCell>
                  <TableCell>{this.props.hike.hiking_time}</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Length</TableCell>
                  <TableCell>{this.props.hike.length} km</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Elevation</TableCell>
                  <TableCell>&#8599;{this.props.hike.height_gain}m &#8600;{this.props.height_loss ? this.props.height_loss : '?'}m</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Highest point</TableCell>
                  <TableCell>{this.props.hike.highest_point ? `${this.props.hike.highest_point}m` : 'N/A'}</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Fitness level</TableCell>
                  <TableCell>{this.props.hike.fitness_level}</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Technique</TableCell>
                  <TableCell>{this.props.hike.technique}</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Webcams</TableCell>
                  <TableCell>{this.props.hike.webcams
                                ? <a href={`${this.props.hike.webcams}`} target="_blank">{this.props.hike.webcams}</a>
                                : 'N/A'}</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Season</TableCell>
                  <TableCell>{this.props.hike.season}</TableCell>
              </TableRow>
              <TableRow>
                  <TableCell component="th" scope="row">Categories</TableCell>
                  <TableCell>{this.props.hike.category ? this.props.hike.category.name : 'N/A'}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </div>
        <div style={styles.reviews}>
        <HikeReviews reviews={this.props.hike.reviews} hikeId={this.props.hike.id} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    hikes: state.hikes.routes,
    hike: state.hikes.routes
          ? state.hikes.routes.find(hike => hike.id === Number(ownProps.match.params.hikeId))
          : undefined
  }
};

export default connect(mapStateToProps)(withRouter(HikePage));
