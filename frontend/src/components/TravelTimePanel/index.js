import React, { Component } from 'react';
import './index.css';
import { connect } from "react-redux";
import { changeTravelTime } from '../../store/actions/mapActions';


class TravelTimePanel extends Component {

    render() {
        console.log('TravelTimePanel props', this.props);
        return (
            <div className='travel-time-panel' id='selectionBar'>
                { this.props.travelTimes.map((time, index) =>
                    <div key={index} onClick={ () => this.changeTime(time) }
                         className={`time-button ${ time === this.props.travelTime ? 'active' : ''}`}>
                     { time/60 } min
                    </div>) }
            </div>
        );
    }

    changeTime = (time) => {
        this.props.dispatch(changeTravelTime(time));
        this.props.showPolygons();
    }
}

const mapStateToProps = (state) => ({
     travelTime: state.map.travelTime,
     travelTimes: state.map.travelTimes,
});

export default connect(mapStateToProps)(TravelTimePanel);
