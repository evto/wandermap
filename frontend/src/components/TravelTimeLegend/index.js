import React, { Component } from 'react';
import './index.css';
import { connect } from "react-redux";
import {polygonColors} from '../../store/constants';


class TravelTimeLegend extends Component {
    constructor(props) {
        super(props);

        this.state = {
            defaultColors: [],
        };
    }

    componentDidMount() {
      this.setState({
        defaultColors: polygonColors.filter(t =>
          this.props.travelTimes.indexOf(t.time) > -1).map(t => t.color)
      });
    }

    render() {
        console.log('TravelTimeLegend props', this.props);
        console.log('TravelTimeLegend state', this.state);
        return (
          <div className='travel-time-legend-container'>
            <div className='time-legend-elements-container'>
            { this.props.travelTimes.map((time, index) =>
              <div key={ index } className='time-legend-element' style={{backgroundColor: this.state.defaultColors[index]}}>
                { time/60 } min
              </div>) }
            </div>
          </div>
        );
    }
}

const mapStateToProps = (state) => ({
     travelTime: state.map.travelTime,
     travelTimes: state.map.travelTimes,
});

export default connect(mapStateToProps)(TravelTimeLegend);
