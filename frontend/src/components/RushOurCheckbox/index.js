import React, { Component } from 'react';
import { connect } from "react-redux";
import { withStyles } from 'material-ui/styles';
import FormControlLabel from 'material-ui/Form/FormControlLabel';
import FormHelperText from 'material-ui/Form/FormHelperText';
import Checkbox from 'material-ui/Checkbox';

import {toggleRushHour} from "../../store/actions/mapActions";
import {CAR} from "../../store/constants";


const styles = {
  root: {
    position: 'absolute',
    right: 10,
    top: 120,
    zIndex: 1000,
    boxShadow: '0 1px 5px rgba(0,0,0,.4)',
    backgroundColor: 'rgba(255,255,255,1)',
    paddingLeft: 10,
    paddingBottom: 10
  }
};

class RushOurCheckbox extends Component {

    componentDidUpdate() {
        if (this.props.travelMode === CAR) {
            this.props.showPolygons();
        }

    }

    handleChange = () => {
      this.props.dispatch(toggleRushHour());
    };


    render() {
        // console.log('Rush hour checkbox props', this.props);
        return (
          <div className='rush-hour-checkbox' style={styles.root}>
            <FormControlLabel
              control={
                <Checkbox
                  disabled={this.props.travelMode !== CAR}
                  checked={ this.props.rushHour }
                  onChange={ this.handleChange }
                  value="checked"
                />
              }
              label="Rush hour"
            />
            <FormHelperText>Car-travel only</FormHelperText>
          </div>
        );
    }
}

const mapStateToProps = (state) => ({
     rushHour: state.map.rushHour,
     travelMode: state.map.travelMode,
});

export default connect(mapStateToProps)(withStyles(styles)(RushOurCheckbox));

