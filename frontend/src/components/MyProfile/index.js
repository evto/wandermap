import React, { Component } from 'react';
import Header from '../../components/Header';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from 'prop-types';
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
// import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
// import ReviewsIcon from '@material-ui/icons/Star';
// import CommentsIcon from '@material-ui/icons/QuestionAnswer';
// import EditIcon from '@material-ui/icons/Edit';

import AppBar from 'material-ui/AppBar';
import Tabs from 'material-ui/Tabs';
import Tab from 'material-ui/Tabs/Tab';
import Typography from 'material-ui/Typography';

import {fetchCurrentUser} from "../../store/actions/currentUserActions";
import UserBio from "../UserBio";
import UserReviews from "../UserReviews";
// import UserComments from "../UserComments";
import HikesCards from '../HikesCards';
import {fetchBookmarkedHikes, fetchFavoriteHikes} from "../../store/actions/hikeActions";


const styles = {
  list: {
    width: '100%',
    maxWidth: 250,
  },
  tabs: {
    width: '100%',
  },
};

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

class MyProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tabValue: 0
        }
    }

    handleChange = (event, tabValue) => {
      this.setState({ tabValue });
    };

    componentDidMount() {
        console.log('in Me component did mount');
        this.props.dispatch(fetchCurrentUser());
        this.props.dispatch(fetchFavoriteHikes());
        this.props.dispatch(fetchBookmarkedHikes());
    }

    render() {
      console.log('Me props', this.props);
      if (!this.props.userData) {
        return <Paper><CircularProgress /></Paper>;
      }
      return (
        <div className='my-profile-container'>
          <Header />
            <UserBio user={this.props.userData}/>
              {/*<List component="nav" style={styles.list}>*/}
                {/*<ListItem button>*/}
                  {/*<ListItemIcon><ReviewsIcon /></ListItemIcon>*/}
                  {/*<ListItemText primary="Reviews" />*/}
                {/*</ListItem>*/}
                {/*<ListItem button>*/}
                  {/*<ListItemIcon><CommentsIcon /></ListItemIcon>*/}
                  {/*<ListItemText primary="Comments" />*/}
                {/*</ListItem>*/}
                 {/*<ListItem button>*/}
                  {/*<ListItemIcon><EditIcon /></ListItemIcon>*/}
                  {/*<ListItemText primary="Edit profile" />*/}
                {/*</ListItem>*/}
              {/*</List>*/}

                {/*<UserComments comments={this.props.userData.comments ? this.props.userData.comments : []}/>*/}
          <div className='user-hikes' style={styles.tabs}>
            <AppBar position="static" color='default'>
              <Tabs value={this.state.tabValue} onChange={this.handleChange} centered>
                <Tab label="reviews" />
                <Tab label="to be hiked" />
                <Tab label="favorites" />
              </Tabs>
            </AppBar>
            {this.state.tabValue === 0 && <TabContainer><UserReviews reviews={this.props.userData.reviews ? this.props.userData.reviews : []}/></TabContainer>}
            {this.state.tabValue === 1 && <TabContainer><HikesCards hikes={this.props.bookmarkedHikes}/></TabContainer>}
            {this.state.tabValue === 2 && <TabContainer><HikesCards hikes={this.props.favoriteHikes}/></TabContainer>}
          </div>
        </div>
      );
    }
}

const mapStateToProps = (state) => {
  console.log('state in my profile', state);
  return {
    userData: state.currentUser.userData,
    favoriteHikes: state.hikes.favorites,
    bookmarkedHikes: state.hikes.bookmarks,
  }
};

export default connect(mapStateToProps)(withRouter(MyProfile));
