import React, { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Header from '../Header';
import {fetchHikes} from "../../store/actions/hikeActions";
import HikesFilter from '../../containers/HikesFilter';
import FiltersPanel from "../FiltersPanel";


const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    height: 'auto',
    padding: 20,
    marginTop: 20,
    fontWeight: 'bold',
    color: '#4A4A4A'
  }
};

class HikesSearchFilter extends Component {

  componentDidMount() {
      this.props.dispatch(fetchHikes());
  }

  render() {
    console.log('hikes search filter props', this.props);
    if (!this.props) {
      return <Paper><CircularProgress /></Paper>;
    }

    return (
      <div className='hikes-cards-container'>
          <Header/>
          <div style={styles.title}>
              <Typography variant="title" color="inherit">
                SEARCH HIKING ROUTES
              </Typography>
          </div>
          <FiltersPanel/>
          <HikesFilter />
      </div>
    );
  }
}

export default connect()(withRouter(HikesSearchFilter));
