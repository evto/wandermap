import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
// import { withStyles } from 'material-ui/styles';

import Card from 'material-ui/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardContent from 'material-ui/Card/CardContent';
import CardMedia from 'material-ui/Card/CardMedia';
import Typography from 'material-ui/Typography';
// import HikeStarRating from "../HikeStarRating";
import LikeDislikeHikeButton from "../LikeDislikeHikeButton";
import BookmarkUnbookmarkHikeButton from "../BookmarkUnbookmarkHikeButton";

import DefaultHikeImage from '../../images/default_hike_image.png';
import PercentageStarRating from "../PercentageStarRating";

const styles = {
  card: {
    // minWidth: 200,
    // maxWidth: 270,
      width: 270
  },
  media: {
    height: 0,
    cursor: 'pointer',
    paddingTop: '56.25%', // 16:9
  },
  link: {
    cursor: 'pointer',
  }
};


class HikeCard extends Component {

  handleClick = () => {
    this.props.history.push(`/hikes/${this.props.hike.id}`);
  };

  render() {
    // console.log('SingleHikeCard props', this.props);
    return (
      <div className='hike-card-container'>
        <Card style={styles.card}>
        <CardMedia
          style={styles.media}
          image={this.props.hike.images.length > 0 ? this.props.hike.images[0].image : DefaultHikeImage}
          title="Sample hike image"
          onClick={this.handleClick}
        />
        <CardContent>
          <Typography gutterBottom component="h4" onClick={this.handleClick} style={styles.link}>
            { this.props.hike.name }
          </Typography>
          {/*<HikeStarRating rating={ this.props.hike.rating }/>*/}
          <PercentageStarRating rating={ this.props.hike.rating }/>
          <Typography component="p">
              { this.props.hike.reviews.length } reviews
          </Typography>
        </CardContent>
        <CardActions>
          <BookmarkUnbookmarkHikeButton hike={this.props.hike}/>
          <LikeDislikeHikeButton hike={this.props.hike}/>
        </CardActions>
      </Card>
      </div>
    );
  }
}

// export default withStyles(styles)(withRouter(HikeCard));
export default withRouter(HikeCard);
