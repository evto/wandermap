let urlBase;
if (process.env.NODE_ENV === 'development') {
   urlBase = `http://localhost:8080/backend`;
} else {
   urlBase = `https://wandermap.ch/backend`;
}
export {urlBase};

export const apikey = "3AYZN7D7EZ42O032I8RW32175124";
export const googleMapsUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';
export const googleMapsKey = 'AIzaSyB1jOpNYPWmda-mirhzvWgfHHT5XsJu-PE';
export const serviceUrl = "https://service.route360.net/germany/";
export const defaultStartPos = [47.378657, 8.539010]; // Zurich HB

export const owmKey = 'ba534645ce13a90b33b5a04a9302532c';

export const WALK = 'walk';
export const BIKE = 'bike';
export const CAR = 'car';
export const TRANSIT = 'transit';

export const GET_HIKES = 'GET_HIKES';
export const GET_HIKE = 'GET_HIKE';
export const GET_TOP_HIKES = 'GET_TOP_HIKES';
export const GET_FAVORITE_HIKES = 'GET_FAVORITE_HIKES';
export const GET_BOOKMARKED_HIKES = 'GET_BOOKMARKED_HIKES';
export const FILL_LIKE = 'FILL_LIKE';
export const UNFILL_LIKE = 'UNFILL_LIKE';
export const FILL_BOOKMARK = 'FILL_BOOKMARK';
export const UNFILL_BOOKMARK = 'UNFILL_BOOKMARK';
export const FILL_USEFUL = 'FILL_USEFUL';
export const UNFILL_USEFUL = 'UNFILL_USEFUL';
export const ADD_FILTER = 'ADD_FILTER';
export const SHOW_HIKE_DRAWER = 'SHOW_HIKE_DRAWER';
export const HIDE_HIKE_DRAWER = 'HIDE_HIKE_DRAWER';

export const CHANGE_TRAVEL_TIME = 'CHANGE_TRAVEL_TIME';
export const CHANGE_TRAVEL_MODE = 'CHANGE_TRAVEL_MODE';
export const TOGGLE_RUSH_HOUR = 'TOGGLE_RUSH_HOUR';
export const ALLOW_LOCATION_ACCESS = 'ALLOW_LOCATION_ACCESS';
export const SET_USER_LOCATION = 'SET_USER_LOCATION';
export const ADD_SOURCE = 'ADD_SOURCE';
export const UPDATE_SOURCES = 'UPDATE_SOURCES';
export const REMOVE_SOURCE = 'REMOVE_SOURCE';
export const CLEAR_SOURCES = 'CLEAR_SOURCES';
export const UPDATE_ADDRESS_BOOK = 'UPDATE_ADDRESS_BOOK';
export const ADD_TARGET = 'ADD_TARGET';
export const REMOVE_TARGET = 'REMOVE_TARGET';
export const CLEAR_TARGETS = 'CLEAR_TARGETS';

export const REGISTER = 'REGISTER';
export const VALIDATE_REGISTRATION = 'VALIDATE_REGISTRATION';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SHOW_ME = 'SHOW_ME';
export const LOGOUT = 'LOGOUT';

export const SUBMIT_REVIEW = 'SUBMIT_REVIEW';

export const selectors = [
    { key: 'hiking_time', name: 'Hiking time, h', choices: ['0 - 2', '2 - 4', '4 - 6', '6 - 8', '8+']},
    { key: 'length', name: 'Length, km', choices: ['0 - 5', '5 - 10', '10 - 15', '15 - 20', '20+']},
    { key: 'fitness_level', name: 'Fitness level', choices: ['EASY', 'MEDIUM', 'HARD']},
    { key: 'technique', name: 'Technique', choices: ['EASY', 'MEDIUM', 'HARD']},
    { key: 'rating', name: 'Review score', choices: ['4.5 +', '4.0 +', '3.5 +', '3.0 +']},
];

export const intersectionMode = { key: 'intersection_mode',
                                  name: 'Intersection mode',
                                  choices: ['Union', 'Difference', 'Average']};

export const polygonColors = [
          { 'time':  900, 'opacity': 0.4, 'color': '#0C6939' },
          { 'time': 1800, 'opacity': 0.4, 'color': '#8DC945' },
          { 'time': 2700, 'opacity': 0.4, 'color': '#F49324' },
          { 'time': 3600, 'opacity': 0.4, 'color': '#BE212C' }
        ];
