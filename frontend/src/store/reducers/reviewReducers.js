import {FILL_USEFUL, UNFILL_USEFUL} from "../constants";

const initialState = {
};

export const reviews = (state = initialState, action) => {
    switch(action.type) {
        case FILL_USEFUL:
            const newRoutes = [...state.routes];
            const reviewHike = newRoutes.find(hike => hike.id === action.payload.hike.id);
            const newReviewsAfterLike = reviewHike.reviews;
            const likedReview = newReviewsAfterLike.find(review => review.id === action.payload.id);
            likedReview['likes_count'] += 1;
            return {...state, routes: newReviewsAfterLike};
        case UNFILL_USEFUL:
            const newReviewsAfterUnlike = state.routes;
            const unlikedReview = newReviewsAfterUnlike.find(review => review.id === action.payload);
            unlikedReview['in_favorites'] = false;
            return {...state, routes: newReviewsAfterUnlike};
        default:
            return state;
    }
};
