import { combineReducers } from 'redux';
import { hikes } from './hikeReducers';
import { map } from './mapReducers';
import { currentUser } from "./currentUserReducers";
import {reviews} from "./reviewReducers";

const mainReducer = combineReducers({
    currentUser,
    map,
    hikes,
    reviews
});

export default mainReducer;
