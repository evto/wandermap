import {
    ADD_SOURCE, ADD_TARGET,
    ALLOW_LOCATION_ACCESS,
    CHANGE_TRAVEL_MODE,
    CHANGE_TRAVEL_TIME, CLEAR_SOURCES, CLEAR_TARGETS,
    defaultStartPos, HIDE_HIKE_DRAWER, REMOVE_SOURCE,
    SET_USER_LOCATION, SHOW_HIKE_DRAWER, TOGGLE_RUSH_HOUR,
    TRANSIT, UPDATE_ADDRESS_BOOK, UPDATE_SOURCES
} from "../constants";

const initialState = {
    userLocationAccess: false,
    userLocation: defaultStartPos,
    sources: [],
    targets: [],
    zoom: 14,
    travelMode: TRANSIT,
    travelTime: 3600,
    // travelTimes: [600, 1200, 1800],
    travelTimes: [900, 1800, 2700, 3600],
    intersectionMode: 'intersection',
    rushHour: false,
    addressBook: {},
    clickedHike: undefined
};

export const map = (state = initialState, action) => {
    switch(action.type) {
        case CHANGE_TRAVEL_MODE:
            console.log("Map Reducer: Changing travel mode");
            return {...state, travelMode: action.payload};
        case CHANGE_TRAVEL_TIME:
            return {...state, travelTime: action.payload};
        case TOGGLE_RUSH_HOUR:
            if (state.rushHour) {
                return {...state, rushHour: false}
            } else {
                return {...state, rushHour: true}
            }
        case ALLOW_LOCATION_ACCESS:
            console.log('in allow access reducer');
            return {...state, userLocationAccess: true};
        case SET_USER_LOCATION:
            return {...state, userLocation: action.payload};
        case ADD_SOURCE:
            const sourceToAdd = {id: state.sources.length, latlng: action.payload};
            console.log('adding source', sourceToAdd);
            const updatedSources = state.sources.concat([sourceToAdd]);
            return {...state, sources: updatedSources};
        case UPDATE_SOURCES:
            const newSources = state.sources;
            console.log('Updating source with index', action.payload.index);
            let sourceToUpdate = newSources.find((source, index) => index === action.payload.index);
            if (sourceToUpdate) {
                console.log('found source to update', sourceToUpdate);
                sourceToUpdate['latlng'] = action.payload.latlng;
            } else {
                console.log('not found source to update');
                newSources.push({latlng: action.payload.latlng});
            }
            return {...state, sources: newSources};
        case UPDATE_ADDRESS_BOOK:
            const key = action.payload.latlng;
            const updatedAddressBook = {...state.addressBook, [key]: action.payload.address};
            return {...state, addressBook: updatedAddressBook};
        case REMOVE_SOURCE:
            const sources = state.sources.filter((source, index) => {
                console.log(`in the remove source reducer, source: ${source} index: ${index}`);
                return index !== action.payload
            });
            return {...state, sources};
        case CLEAR_SOURCES:
            return {...state, sources:[]};
        case ADD_TARGET:
            const targetToAdd = {id: state.targets.length, latlng: action.payload};
            console.log('adding target', targetToAdd);
            const updatedTargets = state.targets.concat([targetToAdd]);
            return {...state, targets: updatedTargets};
        case CLEAR_TARGETS:
            return {...state, targets:[]};
        case SHOW_HIKE_DRAWER:
            console.log('in the show hike drawer');
            return {...state, clickedHike: action.payload};
        case HIDE_HIKE_DRAWER:
            console.log('in the hide hike drawer');
            return {...state, clickedHike: undefined};
        default:
            return state;
    }
};
