import {
    ADD_FILTER, FILL_BOOKMARK,
    FILL_LIKE,
    GET_BOOKMARKED_HIKES,
    GET_FAVORITE_HIKES,
    GET_HIKE,
    GET_HIKES,
    GET_TOP_HIKES, UNFILL_BOOKMARK, UNFILL_LIKE
} from "../constants";

const initialState = {
    filters: {},
};

export const hikes = (state = initialState, action) => {
    switch(action.type) {
        case GET_HIKE:
            const { hike } = action.payload;
            return {...state, hike};
        case GET_HIKES:
            console.log('get hikes reducer');
            return {...state, routes: action.payload};
        case GET_TOP_HIKES:
            console.log('get top hikes reducer');
            return {...state, top: action.payload};
        case GET_FAVORITE_HIKES:
            console.log('get favorite hikes reducer');
            return {...state, favorites: action.payload};
        case GET_BOOKMARKED_HIKES:
            console.log('get bookmarked hikes reducer');
            return {...state, bookmarks: action.payload};
        case FILL_LIKE:
            const newRoutesAfterLike = state.routes;
            const likedHike = newRoutesAfterLike.find(hike => hike.id === action.payload);
            likedHike['in_favorites'] = true;
            return {...state, routes: newRoutesAfterLike};
        case UNFILL_LIKE:
            const newRoutesAfterDislike = state.routes;
            const dislikedHike = newRoutesAfterDislike.find(hike => hike.id === action.payload);
            dislikedHike['in_favorites'] = false;
            return {...state, routes: newRoutesAfterDislike};
        case FILL_BOOKMARK:
            const newRoutesAfterBookmark = state.routes;
            const bookmarkedHike = newRoutesAfterBookmark.find(hike => hike.id === action.payload);
            bookmarkedHike['in_bookmarks'] = true;
            return {...state, routes: newRoutesAfterBookmark};
        case UNFILL_BOOKMARK:
            const newRoutesAfterUnbookmark = state.routes;
            const unbookmarkedHike = newRoutesAfterUnbookmark.find(hike => hike.id === action.payload);
            unbookmarkedHike['in_bookmarks'] = false;
            return {...state, routes: newRoutesAfterUnbookmark};
        case ADD_FILTER:
            const newFilterKey = Object.keys(action.payload)[0];
            const newFilterValue = action.payload[newFilterKey];
            const newFilters = {...state.filters};
            newFilters[newFilterKey] = newFilterValue;
            return {...state, filters: newFilters};
        default:
            return state;
    }
};
