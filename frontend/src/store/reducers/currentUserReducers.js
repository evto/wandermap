import {LOGOUT, SET_CURRENT_USER, SHOW_ME, REGISTER, VALIDATE_REGISTRATION} from "../constants";

const initialState = {
};

export const currentUser = (state = initialState, action) => {
    switch(action.type) {
      case REGISTER:
        return { ...state, registerEmail: action.payload };
      case VALIDATE_REGISTRATION:
        return { ...state, data: action.payload };
      case SET_CURRENT_USER:
        return Object.assign({}, state, action.payload.user);
      case SHOW_ME:
        console.log('in the show me reducer');
        return { ...state, userData: action.payload};
      case LOGOUT:
        localStorage.clear();
        return state;
      default:
        return state;
    }
};
