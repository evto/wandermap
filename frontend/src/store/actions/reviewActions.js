import {FILL_USEFUL, UNFILL_USEFUL, urlBase} from "../constants";

export const fillUsefulIcon = (review) => ({
    type: FILL_USEFUL,
    payload: review
});

export const likeReview = (reviewId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'POST',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/reviews/like/${reviewId}/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch like reviews', data);
        })
    }
};

export const unfillUsefulIcon = (review) => ({
    type: UNFILL_USEFUL,
    payload: review
});

export const unlikeReview = (reviewId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
        const myHeaders = new Headers({
            Authorization: `Bearer ${currentUser.access}`,
        });
        const config = {
            method: 'DELETE',
            headers: myHeaders,
        };

        return fetch(`${urlBase}/api/reviews/like/${reviewId}/`, config)
            .then(response => response.json())
            .then(data => {
                console.log('in the fetch unlike review', data);
            })
    }
};

export const dislikeReview = (reviewId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'POST',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/reviews/dislike/${reviewId}/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch like reviews', data);
        })
    }
};

export const undislikeReview = (reviewId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
        const myHeaders = new Headers({
            Authorization: `Bearer ${currentUser.access}`,
        });
        const config = {
            method: 'DELETE',
            headers: myHeaders,
        };

        return fetch(`${urlBase}/api/reviews/dislike/${reviewId}/`, config)
            .then(response => response.json())
            .then(data => {
                console.log('in the fetch unlike review', data);
            })
    }
};
