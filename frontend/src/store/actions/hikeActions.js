import {
    ADD_FILTER, FILL_BOOKMARK, FILL_LIKE,
    GET_BOOKMARKED_HIKES,
    GET_FAVORITE_HIKES,
    GET_HIKE,
    GET_HIKES,
    GET_TOP_HIKES, UNFILL_BOOKMARK, UNFILL_LIKE,
    urlBase
} from "../constants";

const getHikes = (hikes) => ({
    type: GET_HIKES,
    payload: hikes
});


export const fetchHikes = () => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    let myHeaders = new Headers({});
    if (currentUser.access) {
        myHeaders = new Headers({
            Authorization: `Bearer ${currentUser.access}`,
        });
    }
    const config = {
        method: 'GET',
        headers: myHeaders,
    };

    return fetch(`${urlBase}/api/hikes/`, config)
            .then(response => response.json())
            .then(hikes => {
                console.log('got hikes from fetch', hikes);
                dispatch(getHikes(hikes));
            })
};

export const fetchHike = (hikeId) => (dispatch, getState) => {
    console.log('in fetch hike');
    const headers = new Headers({
            "Content-Type": "application/json"
        });
        const config = {
            method: "GET",
            headers: headers,
        };

    return fetch(`${urlBase}api/hikes/${hikeId}/`, config)
        .then(response => response.json())
        .then(hike => {
            dispatch(getHike(hike));
        })
};

const getHike = (hike) => (
    {
        type: GET_HIKE,
        payload: { hike }
    }
);

const getFavoriteHikes = (hikes) => (
    {
        type: GET_FAVORITE_HIKES,
        payload: hikes
    }
);

export const fetchFavoriteHikes = () => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    // console.log("CurrentUser favorite hikes action", currentUser);
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'GET',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/hikes/favorites/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch favorite hikes', data);
          dispatch(getFavoriteHikes(data));
        })
    }
};

const getBookmarkedHikes = (hikes) => (
    {
        type: GET_BOOKMARKED_HIKES,
        payload: hikes
    }
);

export const fetchBookmarkedHikes = () => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    // console.log("CurrentUser bookmarked hikes action", currentUser);
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'GET',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/hikes/bookmarks/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch bookamrked hikes', data);
          dispatch(getBookmarkedHikes(data));
        })
    }
};

const getTopHikes = (hikes) => (
    {
        type: GET_TOP_HIKES,
        payload: hikes
    }
);

export const fetchTopHikes = () => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    let myHeaders = new Headers({});
    // console.log("CurrentUser bookmarked hikes action", currentUser);
    if (currentUser.access) {
        myHeaders = new Headers({
            Authorization: `Bearer ${currentUser.access}`,
        });
    }
    const config = {
        method: 'GET',
        headers: myHeaders,
    };

    return fetch(`${urlBase}/api/hikes/top/`, config)
        .then(response => response.json())
        .then(data => {
            console.log('in the fetch top hikes', data);
            dispatch(getTopHikes(data));
        })
};

export const fillLikeIcon = (hikeId) => ({
    type: FILL_LIKE,
    payload: hikeId
});

export const likeHike = (hikeId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'POST',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/hikes/like/${hikeId}/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch like hikes', data);
        })
    }
};

export const unfillLikeIcon = (hikeId) => ({
    type: UNFILL_LIKE,
    payload: hikeId
});

export const dislikeHike = (hikeId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'DELETE',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/hikes/like/${hikeId}/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch dislike hikes', data);
        })
    }
};

export const fillBookmarkIcon = (hikeId) => ({
    type: FILL_BOOKMARK,
    payload: hikeId
});

export const bookmarkHike = (hikeId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'POST',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/hikes/bookmark/${hikeId}/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch bookmark hikes', data);
        })
    }
};

export const unfillBookmarkIcon = (hikeId) => ({
    type: UNFILL_BOOKMARK,
    payload: hikeId
});

export const unbookmarkHike = (hikeId) => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'DELETE',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/hikes/bookmark/${hikeId}/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch unbookmark hikes', data);
        })
    }
};

export const submitReview = (hikeId, review) =>(dispatch, getState) => {
    const currentUser = getState().currentUser;
    if (currentUser.access) {
      const myHeaders = new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${currentUser.access}`,
      });
      const config = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify(review),
      };

      return fetch(`${urlBase}/api/reviews/new_review/${hikeId}/`, config)
        .then(response => console.log(response))
        .then(data => {
          console.log('in the submit Review', data);
        })
    }
};

export const addFilter = (filter) => (
    {
        type: ADD_FILTER,
        payload: filter
    }
);
