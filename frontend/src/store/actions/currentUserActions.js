import {LOGOUT, SET_CURRENT_USER, SHOW_ME, urlBase} from "../constants";


export const googleLogin = (credentials) => dispatch => {
    console.log('in google login action');
    const loginUrl = `${urlBase}/api/auth/social-auth-token/backend/`;
    const config = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(credentials),
  };

  return fetch(loginUrl, config)
    .then(response => response.json())
    .then(user => {
      console.log(user);
    });
};

export const register = (email) => dispatch => {
  console.log('In registration action');
  const registrationUrl = `${urlBase}/api/registration/`;
  const config = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(email),
  };

  return fetch(registrationUrl, config)
    .then(response => response.json())
    .then(user => {
      console.log('in the register action', user);
      // localStorage.setItem('userToken', JSON.stringify(user));
      // dispatch(setCurrentUser({access: user.access, refresh: user.refresh}));
    })
    .catch(err => {
      console.log(err);
    })
};

export const validateRegistration = (userData) => dispatch => {
  console.log('In registration validation action', userData);
  const registrationValidationUrl = `${urlBase}/api/registration/validation/`;
  const config = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(userData),
  };

  return fetch(registrationValidationUrl, config)
    .then(response => response.json())
    .then(user => {
      console.log('in the registration validation action', user);
      // localStorage.setItem('userToken', JSON.stringify(user));
      // dispatch(setCurrentUser({access: user.access, refresh: user.refresh}));
    })
    .catch(err => {
      console.log(err);
    })
};

export const login = (credentials) => dispatch => {
  console.log('In login action');
  localStorage.clear();
  const loginUrl = `${urlBase}/api/auth/token/`;
  const config = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(credentials),
  };

  return fetch(loginUrl, config)
    .then(response => response.json())
    .then(user => {
      console.log('fetched auth token', user);
      localStorage.setItem('userToken', JSON.stringify(user));
      dispatch(setCurrentUser({access: user.access, refresh: user.refresh}));
    })
    .catch(err => {
      console.log(err);
    })
};

export const setCurrentUser = (user) => {
  return {
    type: SET_CURRENT_USER,
    payload: { user }
  }
};

export const logout = () => {
  localStorage.clear();
  return {
    type: LOGOUT,
  }
};

export const fetchLocalUser = () => (dispatch) => {
  const userToken = localStorage.getItem('userToken');
  if (userToken && userToken !== 'undefined') {
    console.log('user token is not undefined');
    dispatch(setCurrentUser({ access: JSON.parse(userToken).access,
                              refresh: JSON.parse(userToken).refresh}));
    dispatch(fetchCurrentUser());
  } else {
    console.log('user token is undefined, continue with anonymous user');
  }
};

export const fetchCurrentUser = () => (dispatch, getState) => {
    const currentUser = getState().currentUser;
    console.log("CurrentUser", currentUser);
    if (currentUser.access) {
        console.log('setting headers');
      const myHeaders = new Headers({
        Authorization: `Bearer ${currentUser.access}`,
      });
      // console.log('header', myHeaders.get('Authorization'));
      const config = {
        method: 'GET',
        headers: myHeaders,
      };

      return fetch(`${urlBase}/api/me/`, config)
        .then(response => response.json())
        .then(data => {
          console.log('in the fetch me');
          console.log(data);
          dispatch(showMe(data));
        })
    }
};

export const showMe = (me) => {
  return {
    type: SHOW_ME,
    payload: me
  }
};
