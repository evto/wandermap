import {
    ADD_SOURCE, ADD_TARGET,
    ALLOW_LOCATION_ACCESS,
    CHANGE_TRAVEL_MODE,
    CHANGE_TRAVEL_TIME, CLEAR_SOURCES, CLEAR_TARGETS, googleMapsKey,
    googleMapsUrl, HIDE_HIKE_DRAWER, REMOVE_SOURCE, REMOVE_TARGET,
    SET_USER_LOCATION, SHOW_HIKE_DRAWER, TOGGLE_RUSH_HOUR, UPDATE_ADDRESS_BOOK, UPDATE_SOURCES,
} from "../constants";

export const changeTravelMode = (mode) => ({
    type: CHANGE_TRAVEL_MODE,
    payload: mode
});

export const changeTravelTime = (time) => ({
    type: CHANGE_TRAVEL_TIME,
    payload: time
});

export const toggleRushHour = () => ({
   type: TOGGLE_RUSH_HOUR
});


export const allowLocationAccess = () => ({
    type: ALLOW_LOCATION_ACCESS,
});

export const setUserLocation = (userLocation) => ({
    type: SET_USER_LOCATION,
    payload: userLocation
});

export const updateSources = (latlng, index) => ({
    type: UPDATE_SOURCES,
    payload: {latlng, index}
});

export const updateAddressBook = (latlng, address) => ({
   type: UPDATE_ADDRESS_BOOK,
   payload: {latlng, address}
});

export const addAddress = (latlng) => (dispatch) => {
    const headers = new Headers({
            //"Content-Type": "application/json"
        });
    const config = {
        method: "GET",
        headers: headers,
    };
    console.log('address fetch in addAddress begins');
    return fetch(`${googleMapsUrl}latlng=${latlng}&key=${googleMapsKey}`, config)
        .then(response => response.json())
        .then(data => {
            console.log('got address from Google Maps API', data.results[0].formatted_address);
            dispatch(updateAddressBook(latlng, data.results[0].formatted_address));
        })
};

export const addSource = (latlng) => ({
  type: ADD_SOURCE,
  payload: latlng
});

export const removeSource = (sourceId) => ({
    type: REMOVE_SOURCE,
    payload: sourceId
});

export const clearSources = () => ({
    type: CLEAR_SOURCES,
});

export const addTarget = (latlng) => ({
  type: ADD_TARGET,
  payload: latlng
});

export const removeTarget = (targetId) => ({
    type: REMOVE_TARGET,
    payload: targetId
});

export const showHikeDrawer = (hike) => ({
    type: SHOW_HIKE_DRAWER,
    payload: hike
});

export const hideHikeDrawer = () => ({
    type: HIDE_HIKE_DRAWER,
});

export const clearTargets = () => ({
    type: CLEAR_TARGETS,
});
