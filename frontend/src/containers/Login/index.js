import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import GoogleLogin from 'react-google-login';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import loginImage from '../../images/login_background.jpg';
import { googleLogin, login } from "../../store/actions/currentUserActions";

import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import {clearSources} from "../../store/actions/mapActions";
import Logo from "../../components/Logo";


const styles = {
    loginContainer: {
      width: '100vw',
      height: 'auto',
      margin: '0 auto',

    },
    loginButton: {
      margin: 32,
      backgroundColor: '#238265'
    },
    formContainer: {
      textAlign: 'center',
      backgroundColor: 'rgba(255,255,255, 0.7)',
      width: '40%',
      minWidth: 400,
      maxWidth: 600,
      margin: '0 auto',
      position: 'absolute',
      top: '30vh',
      left: '33vw'
    },
    loginImage: {
      zIndex: -10,
      width: '100vw',
      height: '100%',
      backgroundSize: 'cover',
      backgroundPosition: 'center'

    },
    logoContainer: {
      textAlign: 'center',
      marginTop: 20,
      cursor: 'pointer',
    },
    logo: {
      width: '80%',
      height: 'auto',

    },
};

class Login extends Component {

    constructor(props) {
      super(props);

      this.state = {
        username: '',
        password: '',
      }
    }

    responseGoogle = (response) => {
      console.log(response);
      this.props.dispatch(googleLogin(response));
    };

    handleSubmit = () => {
      this.props.dispatch(login(this.state))
        .then(() => {
          this.props.history.push('/me');
          // this.props.history.push('/hikes/1');
      })
    };

    goToHome = () => {
      this.props.history.push('/');
    };

    changeUsername = (e) => {
      const newUsername = e.currentTarget.value;
      this.setState({ username: newUsername });
    };

    changePassword = (e) => {
      const newPassword = e.currentTarget.value;
      this.setState({ password: newPassword });
    };

    componentDidMount() {
        this.props.dispatch(clearSources());
    }

    render() {
        console.log('Login props', this.props);
        return (
            <div style={styles.loginContainer}>
              <Paper style={styles.formContainer}>
                <div style={styles.logoContainer} onClick={this.goToHome}>
                  <Logo style={styles.logo}/>
                </div>
                <TextField
                  label="Username"
                  value = { this.state.username }
                  onChange={ this.changeUsername }
                  margin="normal"
                /><br />
                <TextField
                  label="Password"
                  value = { this.state.password }
                  onChange={ this.changePassword }
                  margin="normal"
                  type="password"
                /><br />
                <Button variant="raised" label="Log in" color="secondary" style={ styles.loginButton } onClick={ this.handleSubmit }>
                  Log in
                </Button>
                {/*<GoogleLogin*/}
                    {/*clientId="629710093667-ptllj6daeb5j6bqif9n9p2nkm8j00fdb.apps.googleusercontent.com"*/}
                    {/*buttonText="Login with Google"*/}
                    {/*onSuccess={this.responseGoogle}*/}
                    {/*onFailure={this.responseGoogle}*/}
                {/*/>*/}
                <Typography variant='caption' color="primary">
                  Forgot your password?
                </Typography><br/>
              </Paper>
              <img style={styles.loginImage} src={ loginImage } alt="" />
            </div>
        )};
}

const mapStateToProps = (state) => {
  // if (state.currentUser.currentUser && state.currentUser.currentUser.token) {
  //   return {
  //     token: state.currentUser.currentUser.token
  //   };
  // } else {
  //   return {};
  // }
    return state;
};

Login.propTypes = {
  username: PropTypes.string,
  email: PropTypes.string,
  tab: PropTypes.number,
};

export default connect(mapStateToProps)(withRouter(Login));

