import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import GoogleLogin from 'react-google-login';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import loginImage from '../../images/login_background.jpg';
import { googleLogin, register } from "../../store/actions/currentUserActions";

import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';


const styles = {
    loginContainer: {
      width: '100vw',
      height: 'auto',
      margin: '0 auto',
    },
    loginButton: {
      margin: 32,
    },
    formContainer: {
      textAlign: 'center',
      backgroundColor: 'rgba(255,255,255, 0.7)',
      width: '40vw',
      minWidth: 300,
      maxWidth: 400,
      margin: '0 auto',
      position: 'absolute',
      top: '35vh',
      left: '33vw',
      padding: '20px 80px 10px 80px',
    },
    signupImage: {
      zIndex: -10,
      maxWidth: '100vw',
      maxHeight: '100%',
      backgroundSize: 'cover',
      backgroundPosition: 'center'
    }

};

class Signup extends Component {

    constructor(props) {
      super(props);

      this.state = {
        email: '',
      }
    }

    responseGoogle = (response) => {
      console.log(response);
      this.props.dispatch(googleLogin(response));
    };

    handleSubmit = () => {
      this.props.dispatch(register(this.state))
        .then(() => {
          this.props.history.push('/registration/validation');
      })
    };

    changeEmail = (e) => {
      const newEmail = e.currentTarget.value;
      this.setState({ email: newEmail });
    };

    render() {
        console.log('Login props', this.props);
        return (
          <div style={styles.loginContainer}>
            <Paper style={styles.formContainer}>
              <TextField
                label="E-mail"
                value = { this.state.email }
                onChange={ this.changeEmail }
                margin="normal"
                fullWidth
              /><br />
              <Button variant="raised" label="Log in" color="primary" style={ styles.loginButton } onClick={ this.handleSubmit }>
                Sign up
              </Button><br />
              {/*<GoogleLogin*/}
                {/*clientId="629710093667-ptllj6daeb5j6bqif9n9p2nkm8j00fdb.apps.googleusercontent.com"*/}
                {/*buttonText="Continue with Google"*/}
                {/*onSuccess={this.responseGoogle}*/}
                {/*onFailure={this.responseGoogle}*/}
              {/*/>*/}
            </Paper>
            <img style={styles.signupImage} src={ loginImage } alt="" />
          </div>
        )};
}

const mapStateToProps = (state) => {
  if (state.currentUser.currentUser && state.currentUser.currentUser.token) {
    return {
      token: state.currentUser.currentUser.token
    };
  } else {
    // alert("Login required");
    return {};
  }
};

Signup.propTypes = {
  username: PropTypes.string,
  email: PropTypes.string,
  tab: PropTypes.number,
};

export default connect(mapStateToProps)(withRouter(Signup));

