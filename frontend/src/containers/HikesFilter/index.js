import React, { Component } from 'react';
import { connect } from "react-redux";
import CircularProgress from 'material-ui/Progress/CircularProgress';
import Paper from 'material-ui/Paper';

import HikesCards from "../../components/HikesCards";

import {isMatch, areFiltersEmpty} from "../../utils/helpers";
import {fetchHikes} from "../../store/actions/hikeActions";

class HikesFilter extends Component {
    constructor(props) {
      super(props);

      this.filteredHikes = this.props.routes;
    }

    getFilteredHikes = () => {
        if (this.filteredHikes === undefined || areFiltersEmpty(this.props.filters)) {
            console.log('Edge cases detected');
            this.filteredHikes = this.props.routes;
        }
        else if (Object.keys(this.props.filters).length > 0) {
            this.filteredHikes = this.props.routes;  // start filtering all hikes again
            Object.keys(this.props.filters).forEach(key => {
              console.log('filter key in component did update:', key);
              console.log('filter values are: ', this.props.filters[key]);
              if (this.props.filters[key].length === 0) {
                  console.log('this. key values array is empty');
                  return;
              }
              const routes = this.filteredHikes;
              this.filteredHikes = routes.filter(route => isMatch(route, key, this.props.filters[key]))
                                         .map(route => route);
              console.log('set new filtered hikes: ', this.filteredHikes)
            });
        }
    };

    componentDidMount() {
        console.log('In Hikes Filter component did mount');
        this.props.dispatch(fetchHikes);
    }

    render() {
        console.log('Hikes filter props', this.props);
        if (!this.props.routes) {
          return <Paper><CircularProgress /></Paper>;
        }
        this.getFilteredHikes();
        return (
          <div className='hikes-filtering'>
            <HikesCards hikes={this.filteredHikes}/>
          </div>
        );
    };
}

const mapStateToProps = (state) => ({
     routes: state.hikes.routes,
     filters: state.hikes.filters,
});

export default connect(mapStateToProps)(HikesFilter);
