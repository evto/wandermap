import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import loginImage from '../../images/login_background.jpg';

import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import FormControl from 'material-ui/Form/FormControl';
import Input, {InputLabel, InputAdornment} from 'material-ui/Input';
import IconButton from 'material-ui/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { validateRegistration } from "../../store/actions/currentUserActions";


const styles = {
    loginContainer: {
      width: 1440,
      height: 1024,
      margin: '0 auto',
    },
    loginButton: {
        margin: 32,
    },
    formContainer: {
      textAlign: 'center',
      backgroundColor: 'rgba(255,255,255, 0.7)',
      width: '40%',
      minWidth: 350,
      maxWidth: 550,
      margin: '0 auto',
      position: 'absolute',
      top: '30vh',
      left: '30vw'
    },
    loginImage: {
      zIndex: -10,
      maxWidth: '100vw',
      maxHeight: '100%',
    },
    margin: {
      margin: 10,
    },
    title: {
      color: '#4A4A4A'
    }
};

const radix = 10;

class RegistrationValidation extends Component {

    constructor(props) {
      super(props);

      this.state = {
        email: this.props.match.params.email,
        code: parseInt(this.props.match.params.code, radix),
        password: '',
        password_repeat: '',
        first_name: '',
        last_name: '',
        showPassword: false,
        showPasswordRepeat: false,
      }
    }


    handleSubmit = (event) => {
      event.preventDefault();
      this.props.dispatch(validateRegistration(this.state))
        .then(() => {
          this.props.history.push('/login');
      })
    };

    handleChange = prop => event => {
      this.setState({ [prop]: event.target.value });
    };

    handleMouseDownPassword = event => {
      event.preventDefault();
    };

    handleClickShowPassword = () => {
      this.setState({ showPassword: !this.state.showPassword });
    };

    handleClickShowPasswordRepeat = () => {
      this.setState({ showPasswordRepeat: !this.state.showPasswordRepeat });
    };

    form = () => {
        return (
            <div className='validation-form-container'>
              <Paper style={styles.formContainer}>
              <h2 style={styles.title}>Registration</h2>
              <TextField
                label="E-mail"
                value = { this.state.email }
                onChange={ this.handleChange('email') }
                margin="normal"
                style={styles.margin}
              />
              <TextField
                label="Registration code"
                value = { this.state.code }
                onChange={ this.handleChange('code') }
                margin="normal"
                style={ styles.margin }
              /><br />
              <TextField
                label="First name"
                value = { this.state.first_name }
                onChange={ this.handleChange('first_name') }
                margin="normal"
                style={ styles.margin }
              />
              <TextField
                label="Last name"
                value = { this.state.last_name }
                onChange={ this.handleChange('last_name') }
                margin="normal"
                style={ styles.margin }
              /><br />
              <FormControl className='password-input' style={ styles.margin }>
                <InputLabel htmlFor="adornment-password">Password</InputLabel>
                  <Input
                    id="adornment-password"
                    type={ this.state.showPassword ? 'text' : 'password' }
                    value={ this.state.password }
                    onChange={ this.handleChange('password') }
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="Toggle password visibility"
                          onClick={this.handleClickShowPassword}
                          onMouseDown={this.handleMouseDownPassword}
                        >
                          { this.state.showPassword ? <VisibilityIcon /> : <VisibilityOffIcon /> }
                        </IconButton>
                      </InputAdornment>
                    }
                  />
              </FormControl>
              <FormControl className='password-repeat-input' style={styles.margin}>
                <InputLabel htmlFor="adornment-password-repeat">Repeat password</InputLabel>
                  <Input
                    id="adornment-password"
                    type={ this.state.showPasswordRepeat ? 'text' : 'password' }
                    value={ this.state.password_repeat }
                    onChange={ this.handleChange('password_repeat') }
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="Toggle password repeat visibility"
                          onClick={ this.handleClickShowPasswordRepeat }
                          onMouseDown={ this.handleMouseDownPassword }
                        >
                          {this.state.showPasswordRepeat ? <VisibilityIcon /> : <VisibilityOffIcon />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
              </FormControl>
              <Button variant="raised" label="Log in" color="primary" style={ styles.loginButton } onClick={ this.handleSubmit }>
                complete  registration
              </Button><br />
            </Paper>
            </div>
        )
    };

    render() {
        console.log('Registration verification props', this.props);
        return (
          <div style={styles.loginContainer}>
            { this.form() }
            <img style={styles.loginImage} src={ loginImage } alt="" />
          </div>
        )};
}

const mapStateToProps = (state) => {
    return {
        email: state.registerEmail
    }
};

RegistrationValidation.propTypes = {
  email: PropTypes.string,
  code: PropTypes.string,
  password: PropTypes.string,
  password_repeat: PropTypes.string,
  first_name: PropTypes.string,
  last_name: PropTypes.string,
};

export default connect(mapStateToProps)(withRouter(RegistrationValidation));

