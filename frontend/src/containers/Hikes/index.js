import React, { Component } from 'react';
import { connect } from "react-redux";
import {isMatch, areFiltersEmpty} from "../../utils/helpers";

class Hikes extends Component {
    constructor(props) {
      super(props);

      this.filteredHikes = this.props.routes;
    }

    getFilteredHikes = () => {
        if (this.filteredHikes === undefined || areFiltersEmpty(this.props.filters)) {
            console.log('Edge cases detected');
            this.filteredHikes = this.props.routes;
        }
        else if (Object.keys(this.props.filters).length > 0) {
            this.filteredHikes = this.props.routes;  // start filtering all hikes again
            Object.keys(this.props.filters).forEach(key => {
              console.log('filter key in component did update:', key);
              console.log('filter values are: ', this.props.filters[key]);
              if (this.props.filters[key].length === 0) {
                  console.log('this. key values array is empty');
                  return;
              }
              const routes = this.filteredHikes;
              this.filteredHikes = routes.filter(route => isMatch(route, key, this.props.filters[key]))
                                         .map(route => route);
              console.log('set new filtered hikes: ', this.filteredHikes)
            });
        }
    };

    componentDidUpdate() {
        this.getFilteredHikes();
        console.log('in hikes component did update, state:', this.state);
        if (this.props.routes) {
          this.props.drawHikes(this.filteredHikes);
        }
    };

    render() {
        console.log('Hikes props', this.props);
        return (
            <div className='hikes'> </div>
        );
    };
}

const mapStateToProps = (state) => ({
    routes: state.hikes.routes,
    filters: state.hikes.filters,
});

export default connect(mapStateToProps)(Hikes);
