export const isMatch = (route, key, filterValues) => {
    if (key === 'rating') {
        const ratingValue = filterValues.map(val => Number(val.replace(/[^0-9\.]+/g,""))).sort()[0];
        return route[key] >= ratingValue;
    } else if (key === 'length') {
        const filterRanges = filterValues.map(value => value.split(' - ').map(n => Number(n)));
        for (let range of filterRanges) {
            if (route[key] >= range[0] && route[key] < range[1]) {
                return true;
            }
        }
        return false;
    } else if (key === 'hiking_time') {
        const hikingTimeValue = Number(route[key].substr(0, route[key].indexOf('h')));
        const filterRanges = filterValues.map(value => value.split(' - ').map(n => Number(n)));
        for (let range of filterRanges) {
            if (hikingTimeValue >= range[0] && hikingTimeValue < range[1]) {
                return true;
            }
        }
        return false;
    }
    return filterValues.includes(route[key]);
};

export const areFiltersEmpty = (filters) => {
  for (let key in filters) {
    if (filters[key].length > 0) {
      console.log('filter ARE NOT empty: ', filters);
      return false;
    }
  }
  console.log('filters ARE empty: ', filters);
  return true;
};

// export const getRandomFromArray = (arr) => {
//     return arr[Math.floor(Math.random() * arr.length)];
// };
