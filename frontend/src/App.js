import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';

import store from './store';
import Home from './components/Home';
import { fetchHikes } from "./store/actions/hikeActions";
import Login from "./containers/Login";
import Signup from "./containers/Signup";
import HikesSearchFilter from "./components/HikesSearchFilter";
import RegistrationValidation from "./containers/RegistrationValidation";
import HikePage from "./components/HikePage";
import TopHikes from "./components/TopHikes";
import MyProfile from "./components/MyProfile";
import {fetchLocalUser} from "./store/actions/currentUserActions";

store.dispatch(fetchLocalUser());
store.dispatch(fetchHikes());
const theme = createMuiTheme();

class App extends Component {

  render() {
    return (
      <Provider store={ store }>
        <MuiThemeProvider theme={ theme }>
            <Router>
              <Switch>
                <Route exact path='/' component={ Home } />
                <Route exact path='/login' component={ Login } />
                <Route exact path='/signup' component={ Signup } />
                <Route exact path='/registration/validation/:email/:code' component={ RegistrationValidation } />
                <Route exact path='/me' component={ MyProfile } />
                <Route exact path='/hikes/:hikeId' component={ HikePage } />
                <Route exact path='/top' component={ TopHikes } />
                <Route exact path='/filter' component={ HikesSearchFilter } />
              </Switch>
            </Router>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
