## General

* A user should be able to create a new account by providing only the email address.
* A user should be able to update his profile:
    * `Username`, `First name`, `Last name`, `Email`,  `Password`, `Location`, `Phone` and `Profile Description`.
* A user should be able to log in.
* A user should be able to log out.
* A user should be able to delete its account.


## Anonymous user can do:

* View the list of the hikes.
* View the details of a hike.
* View the contact page of Wandermap project.
* View the about page of Wandermap project.
* Search/Filter for hikes by string or category.
* Search/Filter for reviews by string or category of the hike.
* Sort reviews by highest/lowest rating (by default reviews sorted newest first)
* Register himself on the website.


## Registered user can do:

* Create a hike route (pro users).
* Create a review of a hike route.
* View/Update/Delete a hike review of himself.
* Like/unlike a hike review.
* Create a comment on a hike review.
* Like/unlike a comment.
* Reset his password.
* Update his userprofile.
* Delete his profile.
* Delete the hike route he created (pro users).


# Database

## A hike route contains

* ID
* Name (default: "{Starting point} - {Destination}")
* Category
* Country
* Starting point
* Destination
* Hiking time (range in hours)
* Length (range in km)
* Elevation (descent, ascent)
* Highest point
* Fitness level (easy, medium, hard)
* Technique (easy, medium, hard)
* Season (example: Jun to Sept)
* Cable car (required, optional, no)
* Images (min 3)
* Webcam links
* Notes

## A hike review contains

* ID
* Text-Content
* Raiting | 1-5 stars
* Date created
* Date modified
* User
* Hike
* Likes
* Comments


## User contains

* ID
* Username
* First name
* Last name
* Email
* Location
* Status or badges (moderator, frequent hiker, etc.)
* Things I love
* Description
* Joined date
* Profile picture


## Comments on a review contains

* ID
* User
* Review
* Text content
* Date created
* Date modified
* Likes

___

# REST API backend endpoints


### Registration

* `/api/registration/` POST: Register new user by asking for an email (send email validation code).
* `/api/registration/validate/` POST: Validate a new registred user with validation code sent by email. 


### Auth

* `/api/auth/token/` POST: Get a new JWT by passing username and password.
* `/api/auth/token/refresh/` POST: Get a new JWT by passing an old still valid JWT.
* `/api/auth/token/verify/` POST: Verify a token by passing the token in the body.
* `/api/auth/password-reset/` POST: Reset users password by sending a validation code in a email.
* `/api/auth/password-reset/validate/` POST: Validate password reset token and set new password for the user.


### Search

* `/api/search/` POST: Search for 'hikes', 'reviews' or 'users'. {type: 'hikes', 'search_string': 'pizol'}


### Top (15, 30, 45)

* `/api/top/` GET: Get a list of N best rated hikes.


### Hike

* `/api/hikes/` GET: Get the list of all the restaurant.
* `/api/hikes/new/` POST": Create a new hiking route.
* `/api/hikes/category/<int:category_id>/` GET: Get the all the hikes by category.
* `/api/hikes/user/<int:user_id>/` GET: Get all the hikes created by a specific user in chronological order.
* `/api/hikes/<int:id>/` GET: Get the details of a hike providing the id of the hike.
* `/api/hikes/<int:id>/` POST: Update a hike by id (only by owner or admin).
* `/api/hikes/<int:id>/` DELETE: Delete a hike by id (only by owner or admin).


### Reviews

* `/api/reviews/new/<int:hike_id>/` POST: Create new review for a hike.
* `/api/reviews/hike/<int:hike_id>/` GET: Get the list of the reviews for a single hike.
* `/api/reviews/user/<int:user_id>/` GET: Get the list of the reviews by a single user.
* `/api/reviews/<int:review_id>/` GET: Get a specific review by ID and display all the information.
* `/api/reviews/<int:review_id>/` POST: Update a specific review (only by owner).
* `/api/reviews/<int:review_id>/` DELETE: Delete a specific review (only by owner).
* `/api/reviews/like/<int:review_id>/` POST: Like a review.
* `/api/reviews/like/<int:review_id>/` DELETE: Remove like from the review.
* `/api/reviews/likes/` GET: Get the list of the reviews the current user liked.
* `/api/reviews/comments/` GET: Get the list of the reviews the current user commented.


### Comments

* `/api/comments/<int:user_id>/` GET: Get all the comments from a single user.
* `/api/comments/new/<int:review_id>/` POST: Comment on the review.
* `/api/comments/<int:review_id>/` DELETE: Delete the comment on the review.
* `/api/comments/like/<int:comment_id>/` POST: Like a comment.
* `/api/comments/like/<int:comment_id>/` DELETE: Remove the like from the comment.


### Categories

* `/api/category/list/` GET: Get the list of all the categories.


### Users

* `/api/me/` GET: Get the userprofile.
* `/api/me/` POST: Update the userprofile.
* `/api/users/list/` GET: Get all users.
* `/api/users/?search=<str:search_string>/` GET: Search for a user.
* `/api/users/<int:user_id>/` GET: specific userprofile.
