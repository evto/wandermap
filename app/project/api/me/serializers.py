from django.contrib.auth.models import User
from rest_framework import serializers

from project.api.comments.serializers import CommentSerializer
from project.api.reviews.serializers import ReviewSerializer
from project.hikes.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['location', 'image', 'things_love', 'description']


class MeSerializer(serializers.ModelSerializer):
    """
    Logged in user profile serializer to return the user details
    """

    profile = ProfileSerializer()

    username = serializers.CharField(
        required=False,
        allow_blank=False,
    )
    reviews = ReviewSerializer(read_only=True, many=True)
    comments = CommentSerializer(read_only=True, many=True)

    def validate_username(self, username):
        try:
            User.objects.get(username=username)
            raise serializers.ValidationError('Username is already taken!')
        except User.DoesNotExist:
            return username

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'profile', 'reviews', 'comments']
        read_only_fields = ['id']
