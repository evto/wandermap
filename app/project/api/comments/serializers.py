from rest_framework import serializers

from project.hikes.models import Comment, Review


class CommentSerializer(serializers.ModelSerializer):
    likes = serializers.SerializerMethodField(read_only=True)
    review = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Comment
        fields = ['id', 'user', 'review', 'content', 'modified', 'likes']
        read_only_fields = ['id', 'user', 'review', 'modified', 'likes']

    def create(self, validated_data):
        return Comment.objects.create(
            **validated_data,
            user=self.context.get('request').user,
            review=self.context.get('review')
        )

    def get_likes(self, comment):
        return comment.likes.count()

    def get_review(self, comment):
        review = Review.objects.get(id=comment.review.id)
        return {'id': review.id,
                'content': review.content,
                'modified': review.modified,
                'user': review.user.id
                }
