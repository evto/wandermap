from django.urls import path

from project.api.comments.views import CommentReviewView, UserCommentsView, DeleteReviewCommentView, \
    LikeUnlikeReviewCommentView

app_name = 'comments'

urlpatterns = [
    path('new/<int:review_id>/', CommentReviewView.as_view(), name='new'),
    path('user/<int:user_id>/', UserCommentsView.as_view(), name='user_comments'),
    path('<int:review_id>/', DeleteReviewCommentView.as_view(), name='delete_comment'),
    path('like/<int:comment_id>/', LikeUnlikeReviewCommentView.as_view(), name='like_unlike_comment'),
]
