from django.urls import path

from project.api.home.views import HomeView

app_name = 'home'

urlpatterns = [
    path('', HomeView.as_view()),
]
