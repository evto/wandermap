from rest_framework.generics import ListAPIView

from project.api.hike.serializers import HikeSerializer
# from project.api.permissions import IsOwnerOrReadOnly
from project.hikes.models import Hike


class HomeView(ListAPIView):
    serializer_class = HikeSerializer
    queryset = Hike.objects.all()
    permission_classes = []

    def filter_queryset(self, queryset):

        return queryset
