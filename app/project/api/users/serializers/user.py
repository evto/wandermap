from django.contrib.auth.models import User
from rest_framework import serializers

from project.hikes.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['location', 'image', 'things_love', 'description']


class UserSerializer(serializers.ModelSerializer):
    """
    Logged in user profile serializer to return the user details
    """

    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'profile', 'reviews', 'comments']
        read_only_fields = ['id']
