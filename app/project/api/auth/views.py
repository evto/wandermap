from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from .serializers import PasswordResetSerializer, PasswordResetValidationSerializer
from django.contrib.auth import login
from django.http import HttpResponse

from social_django.utils import psa

# Define an URL entry to point to this view, call it passing the
# access_token parameter like ?access_token=<token>. The URL entry must
# contain the backend, like this:
#
#   url(r'^register-by-token/(?P<backend>[^/]+)/$',
#       'register_by_access_token')


@psa('social:complete')
def register_by_access_token(request, backend):
    # This view expects an access_token GET parameter, if it's needed,
    # request.backend and request.strategy will be loaded with the current
    # backend and strategy.
    token = request.GET.get('access_token')
    user = request.backend.do_auth(token)
    if user:
        login(request, user)
        return HttpResponse('OK')
    else:
        return HttpResponse('ERROR')


class PasswordResetView(GenericAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = PasswordResetSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data.get('email')
        user.user_profile.generate_new_code()
        serializer.send_password_reset_email(user.email, user.user_profile.code)
        return Response(f'Password reset sent to email {user.email}!')


class PasswordResetValidationView(GenericAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = PasswordResetValidationSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(serializer.validated_data)
        return Response(f'New password was set!')
