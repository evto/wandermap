from django.db.models import Avg, Count
from rest_framework import status
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from project.api.base import GetObjectMixin
from project.api.hike.serializers import HikeSerializer
from project.api.permissions import IsOwnerOrReadOnly
from project.hikes.models import Hike, FavoriteHike, BookmarkedHike


class ListAllHikesView(ListAPIView):
    serializer_class = HikeSerializer
    queryset = Hike.objects.all()
    permission_classes = []

    def filter_queryset(self, queryset):
        return queryset.order_by('-modified')


class ListTopHikesView(ListAPIView):
    serializer_class = HikeSerializer
    queryset = Hike.objects.all()
    permission_classes = []

    def filter_queryset(self, queryset):
        return queryset.annotate(
            number_reviews=Count('reviews')
        ).exclude(number_reviews=0).annotate(rating=Avg('reviews__rating')).order_by('-rating')


class LikeDislikeHikeView(GetObjectMixin, GenericAPIView):
    serializer_class = HikeSerializer
    queryset = Hike.objects.all()

    def post(self, request, **kwargs):
        hike = self.get_object()
        FavoriteHike.objects.get_or_create(
            user=request.user,
            hike=hike
        )
        return Response('Hike liked!')

    def delete(self, request, **kwargs):
        hike = self.get_object()
        like = self.get_object_by_model(FavoriteHike, hike=hike, user=request.user)
        like.delete()
        return Response('Like removed!')


class BookmarkUnbookmarkHikeView(GetObjectMixin, GenericAPIView):
    serializer_class = HikeSerializer
    queryset = Hike.objects.all()

    def post(self, request, **kwargs):
        hike = self.get_object()
        BookmarkedHike.objects.get_or_create(
            user=request.user,
            hike=hike
        )
        return Response('Hike bookmarked!')

    def delete(self, request, **kwargs):
        hike = self.get_object()
        like = self.get_object_by_model(BookmarkedHike, hike=hike, user=request.user)
        like.delete()
        return Response('Bookmark removed!')


class UserFavoriteHikesView(ListAPIView):
    serializer_class = HikeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Hike.objects.filter(favorites__user=self.request.user)


class UserBookmarkedHikesView(ListAPIView):
    serializer_class = HikeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Hike.objects.filter(bookmarks__user=self.request.user)


class HikeGetUpdateDeleteView(GenericAPIView):
    queryset = Hike.objects.all()
    serializer_class = HikeSerializer
    permission_classes = [
        IsOwnerOrReadOnly,
    ]

    def get(self, request, **kwargs):
        hike = self.get_object()
        serializer = self.get_serializer(hike)
        return Response(serializer.data, status.HTTP_200_OK)

    def post(self, request, **kwargs):
        hike = self.get_object()
        serializer = self.get_serializer(hike, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)

    def delete(self, request, **kwargs):
        hike = self.get_object()
        hike.delete()
        return Response('Deleted', status.HTTP_200_OK)
