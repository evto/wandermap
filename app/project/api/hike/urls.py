from django.urls import path

from project.api.hike.views import ListAllHikesView, HikeGetUpdateDeleteView, ListTopHikesView, UserFavoriteHikesView, \
    UserBookmarkedHikesView, LikeDislikeHikeView, BookmarkUnbookmarkHikeView

app_name = 'hikes'

urlpatterns = [
    path('', ListAllHikesView.as_view(), name='all'),
    path('top/', ListTopHikesView.as_view(), name='top'),
    # path('?search=<str:search_string>', ListAllHikesView.as_view(), name='search_hike'),
    # path('category/<int:pk>/', ListCategoryHikesView.as_view(), name='category_hikes'),
    path('bookmark/<int:pk>/', BookmarkUnbookmarkHikeView.as_view(), name='bookmark_unbookmark_hike'),
    path('like/<int:pk>/', LikeDislikeHikeView.as_view(), name='like_dislike_hike'),
    path('favorites/', UserFavoriteHikesView.as_view(), name='favorite_hikes'),
    path('bookmarks/', UserBookmarkedHikesView.as_view(), name='bookmarked_hikes'),
    path('<int:pk>/', HikeGetUpdateDeleteView.as_view(), name='get_update_delete_hike'),
]
