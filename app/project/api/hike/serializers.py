from django.db.models import Avg
from rest_framework import serializers

from project.api.categories.serializers import CategorySerializer
from project.api.hike_image.serializers import HikeImageSerializer
from project.api.reviews.serializers import ReviewSerializer
from project.hikes.models import Hike


class HikeSerializer(serializers.ModelSerializer):
    reviews = ReviewSerializer(read_only=True, many=True)
    images = HikeImageSerializer(read_only=True, many=True)
    category = CategorySerializer(read_only=True)
    rating = serializers.SerializerMethodField()
    in_favorites = serializers.SerializerMethodField()
    in_bookmarks = serializers.SerializerMethodField()

    class Meta:
        model = Hike
        fields = ['id', 'name', 'starting_point', 'destination', 'route', 'hiking_time', 'length', 'images', 'webcams',
                  'highest_point', 'fitness_level', 'technique', 'season', 'category', 'user', 'reviews', 'rating',
                  'in_favorites', 'in_bookmarks', 'height_gain', 'height_loss']
        read_only_fields = ['id', 'user', 'reviews', 'rating']

    def create(self, validated_data):
        return Hike.objects.create(
            **validated_data,
            user=self.context.get('request').user
        )

    def get_rating(self, obj):
        average = obj.reviews.aggregate(Avg('rating')).get('rating__avg')
        if average is None:
            return 0
        return average

    def get_in_favorites(self, obj):
        in_favorites = False
        user = self.context.get('request').user
        if user and not user.is_anonymous:
            in_favorites = Hike.objects.filter(favorites__user=user, id=obj.id).exists()
        return in_favorites

    def get_in_bookmarks(self, obj):
        in_bookmarks = False
        user = self.context.get('request').user
        if user and not user.is_anonymous:
            in_bookmarks = Hike.objects.filter(bookmarks__user=user, id=obj.id).exists()
        return in_bookmarks
