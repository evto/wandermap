from rest_framework import status
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from project.api.base import GetObjectMixin
from project.api.permissions import IsOwnerOrReadOnly
from project.api.reviews.serializers import ReviewSerializer
from project.hikes.models import Hike, Review, ReviewLike, ReviewDislike


class NewReviewView(GetObjectMixin, GenericAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = [
        IsAuthenticated,
    ]

    def post(self, request, **kwargs):
        hike = self.get_object_by_model(Hike, pk=self.kwargs.get('pk'))
        request.hike = hike
        serializer = self.get_serializer(data=request.data,
                                         context={'request': request})
        serializer.is_valid(raise_exception=True)
        new_review = serializer.create(serializer.validated_data)
        return Response(ReviewSerializer(new_review).data, status.HTTP_201_CREATED)


class HikeReviewsView(GetObjectMixin, ListAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = [
        IsOwnerOrReadOnly,
    ]

    def filter_queryset(self, queryset):
        hike = self.get_object_by_model(Hike, pk=self.kwargs.get('pk'))
        return queryset.filter(hike=hike)


class UserReviewsView(ListAPIView):
    serializer_class = ReviewSerializer

    def get_queryset(self):
        return Review.objects.filter(user__username=self.request.user.username)


class ReviewGetUpdateDeleteView(GenericAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [
        IsOwnerOrReadOnly,
    ]

    def get(self, request, **kwargs):
        review = self.get_object()
        serializer = self.get_serializer(review)
        return Response(serializer.data, status.HTTP_200_OK)

    def post(self, request, **kwargs):
        review = self.get_object()
        serializer = self.get_serializer(review, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)

    def delete(self, request, **kwargs):
        review = self.get_object()
        review.delete()
        return Response('Deleted')


class LikeUnlikeReviewView(GetObjectMixin, GenericAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = [
        IsAuthenticated,
    ]

    def post(self, request, **kwargs):
        review = self.get_object()
        try:
            dislike = ReviewDislike.objects.get(review=review, user=request.user)
            dislike.delete()
        except ReviewDislike.DoesNotExist:
            pass
        ReviewLike.objects.get_or_create(
            user=request.user,
            review=review
        )
        return Response('Review is useful!')

    def delete(self, request, **kwargs):
        review = self.get_object()
        like = self.get_object_by_model(ReviewLike, review=review, user=request.user)
        like.delete()
        return Response('Review is not useful anymore!')


class DislikeUndislikeReviewView(GetObjectMixin, GenericAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    permission_classes = [
        IsAuthenticated,
    ]

    def post(self, request, **kwargs):
        review = self.get_object()
        try:
            like = ReviewLike.objects.get(review=review, user=request.user)
            like.delete()
        except ReviewLike.DoesNotExist:
            pass
        ReviewDislike.objects.get_or_create(
            user=request.user,
            review=review
        )
        return Response('Review is not useful!')

    def delete(self, request, **kwargs):
        review = self.get_object()
        like = self.get_object_by_model(ReviewDislike, review=review, user=request.user)
        like.delete()
        return Response('Review is ! not useful anymore!')


class LikedReviewsView(ListAPIView):
    serializer_class = ReviewSerializer
    permission_classes = [
        IsAuthenticated,
    ]

    def get_queryset(self):
        return Review.objects.filter(likes__user=self.request.user)


class CommentedReviewsView(ListAPIView):
    serializer_class = ReviewSerializer
    permission_classes = [
        IsAuthenticated,
    ]

    def get_queryset(self):
        return Review.objects.filter(comments__user=self.request.user)
