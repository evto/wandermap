from rest_framework import serializers

from project.api.comments.serializers import CommentSerializer
from project.api.users.serializers.user import UserSerializer
from project.hikes.models import Review, Hike


class ReviewSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(read_only=True, many=True)
    user = UserSerializer(read_only=True)
    hike = serializers.SerializerMethodField(read_only=True)
    liked = serializers.SerializerMethodField()
    disliked = serializers.SerializerMethodField()

    class Meta:
        model = Review
        fields = ['id', 'user', 'hike', 'content', 'rating', 'comments', 'created', 'modified', 'liked', 'disliked']
        read_only_fields = ['id', 'user', 'hike', 'comments', 'created', 'modified']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return {
            **data,
            'like_count': instance.likes.count(),
            'dislike_count': instance.dislikes.count()
        }

    def create(self, validated_data):
        return Review.objects.create(
            **validated_data,
            user=self.context.get('request').user,
            hike=self.context.get('request').hike
        )

    def get_hike(self, obj):
        hike = Hike.objects.get(id=obj.hike.id)
        return {'id': hike.id,
                'name': hike.name,
                }

    def get_liked(self, obj):
        liked = False
        user = self.context.get('request').user
        if user and not user.is_anonymous:
            liked = Review.objects.filter(likes__user=user, id=obj.id).exists()
        return liked

    def get_disliked(self, obj):
        disliked = False
        user = self.context.get('request').user
        if user and not user.is_anonymous:
            disliked = Review.objects.filter(dislikes__user=user, id=obj.id).exists()
        return disliked
