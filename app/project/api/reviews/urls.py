from django.urls import path

from project.api.reviews.views import NewReviewView, HikeReviewsView, UserReviewsView, ReviewGetUpdateDeleteView, \
    LikeUnlikeReviewView, LikedReviewsView, CommentedReviewsView, DislikeUndislikeReviewView

app_name = 'reviews'

urlpatterns = [
    path('new_review/<int:pk>/', NewReviewView.as_view(), name='new'),
    path('hike/<int:pk>/', HikeReviewsView.as_view(), name='hike_reviews'),
    path('user/<int:pk>/', UserReviewsView.as_view(), name='user_reviews'),
    path('<int:pk>/', ReviewGetUpdateDeleteView.as_view(), name='get_update_delete_review'),
    path('like/<int:pk>/', LikeUnlikeReviewView.as_view(), name='like_unlike'),
    path('dislike/<int:pk>/', DislikeUndislikeReviewView.as_view(), name='dislike_undislike'),
    path('likes/', LikedReviewsView.as_view(), name='liked'),
    path('comments/', CommentedReviewsView.as_view(), name='commented'),
]
