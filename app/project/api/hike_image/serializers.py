from rest_framework import serializers

from project.hikes.models.hike import HikeImage


class HikeImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = HikeImage
        fields = ['id', 'hike', 'image']
        read_only_fields = ['id', 'hike']

    def create(self, validated_data):
        return HikeImage.objects.create(
            **validated_data,
            user=self.context.get('request').user,
            hike=self.context.get('request').hike
        )
