from django.urls import path

from project.api.hike_image.views import HikeImageUploadView

app_name = 'hike_images'

urlpatterns = [
    # path('', ListAllHikeImagesView.as_view(), name='all'),
    path('new/', HikeImageUploadView.as_view(), name='new_hike_image'),
]
