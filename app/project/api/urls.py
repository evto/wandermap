from django.urls import path, include

app_name = 'api'
urlpatterns = [
    path('categories/', include('project.api.categories.urls'), name='categories'),
    path('hikes/', include('project.api.hike.urls'), name='hikes'),
    path('reviews/', include('project.api.reviews.urls'), name='reviews'),
    path('comments/', include('project.api.comments.urls'), name='comments'),
    path('registration/', include('project.api.registration.urls'), name='registration'),
    path('users/', include('project.api.users.urls'), name='users'),
    path('me/', include('project.api.me.urls'), name='me'),
    path('auth/', include('project.api.auth.urls'), name='auth'),
    path('home/', include('project.api.home.urls'), name='home'),
]
