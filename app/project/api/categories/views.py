from rest_framework.generics import ListAPIView

from project.api.categories.serializers import CategorySerializer
from project.api.permissions import IsOwnerOrReadOnly
from project.hikes.models import Category


class ListCategoriesView(ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = [IsOwnerOrReadOnly]

    def filter_queryset(self, queryset):
        return queryset
