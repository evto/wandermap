from django.urls import path

from project.api.categories.views import ListCategoriesView

app_name = 'categories'

urlpatterns = [
    path('', ListCategoriesView.as_view(), name='list'),
]
