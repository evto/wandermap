from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models


class Hike(models.Model):

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='hikes',
        null=True,
    )

    name = models.CharField(
        verbose_name='hike_name',
        max_length=100,
    )

    starting_point = ArrayField(
        models.DecimalField(
            decimal_places=6,
            max_digits=9),
        size=2
    )

    destination = ArrayField(
        models.DecimalField(
            decimal_places=6,
            max_digits=9),
        size=2
    )

    route = models.TextField(
        verbose_name='route',
        max_length=300,
    )

    hiking_time = models.CharField(
        verbose_name='hiking_time',
        max_length=20,
    )

    height_gain = models.PositiveIntegerField(
        verbose_name='height_gain_m',
        null=True,
        blank=True,
    )

    height_loss = models.PositiveIntegerField(
        verbose_name='height_loss_m',
        null=True,
        blank=True,
    )

    length = models.DecimalField(
        verbose_name='length_km',
        decimal_places=1,
        max_digits=5
    )

    highest_point = models.PositiveIntegerField(
        verbose_name='highest_point_m',
        null=True,
        blank=True,
    )

    webcams = models.URLField(
        verbose_name='webcams_link',
        blank=True,
    )

    EASY = 'EASY'
    MEDIUM = 'MEDIUM'
    HARD = 'HARD'
    LEVEL_CHOICES = (
        (EASY, 'Easy'),
        (MEDIUM, 'Medium'),
        (HARD, 'Hard'),
    )

    fitness_level = models.CharField(
        verbose_name='fitness_level',
        choices=LEVEL_CHOICES,
        max_length=6,
    )

    technique = models.CharField(
        verbose_name='technique',
        choices=LEVEL_CHOICES,
        max_length=6,
    )

    season = models.CharField(
        verbose_name='season',
        max_length=30,
    )

    # cable_car

    category = models.ForeignKey(
        verbose_name='category',
        to='hikes.Category',
        on_delete=models.SET_NULL,
        related_name='hikes',
        null=True,
        blank=True,
    )

    created = models.DateTimeField(
        verbose_name='date_created',
        auto_now_add=True,
    )

    modified = models.DateTimeField(
        verbose_name='date_modified',
        auto_now=True,
    )

    class Meta:
        verbose_name = 'Hike'
        verbose_name_plural = 'Hikes'


class HikeImage(models.Model):
    hike = models.ForeignKey(
        verbose_name='hike',
        to='hikes.Hike',
        related_name='images',
        on_delete=models.CASCADE,
    )

    image = models.ImageField()


class FavoriteHike(models.Model):
    hike = models.ForeignKey(
        verbose_name='hike',
        to='hikes.Hike',
        related_name='favorites',
        on_delete=models.CASCADE,
    )

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
    )

    class Meta:
        verbose_name = 'Favorite Hike'
        verbose_name_plural = 'Favorite Hikes'
        unique_together = [(
             'user', 'hike'
        )]


class BookmarkedHike(models.Model):
    hike = models.ForeignKey(
        verbose_name='hike',
        to='hikes.Hike',
        related_name='bookmarks',
        on_delete=models.CASCADE,
    )

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
    )

    class Meta:
        verbose_name = 'Bookmarked Hike'
        verbose_name_plural = 'Bookmarked Hikes'
        unique_together = [(
            'user', 'hike'
        )]
