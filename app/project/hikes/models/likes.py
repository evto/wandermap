from django.conf import settings
from django.db import models


class ReviewLike(models.Model):
    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        related_name='review_likes',
        on_delete=models.SET_NULL,
        null=True,
    )

    review = models.ForeignKey(
        verbose_name='review',
        to='hikes.Review',
        related_name='likes',
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'Review like'
        verbose_name_plural = 'Review likes'
        unique_together = [(
             'user', 'review'
        )]


class ReviewDislike(models.Model):
    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        related_name='review_dislikes',
        on_delete=models.SET_NULL,
        null=True,
    )

    review = models.ForeignKey(
        verbose_name='review',
        to='hikes.Review',
        related_name='dislikes',
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'Review dislike'
        verbose_name_plural = 'Review dislikes'
        unique_together = [(
             'user', 'review'
        )]


class CommentLike(models.Model):
    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
    )

    comment = models.ForeignKey(
        verbose_name='comment',
        to='hikes.Comment',
        related_name='likes',
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'CommentLike'
        verbose_name_plural = 'Comment likes'
        unique_together = [(
             'user', 'comment'
        )]
