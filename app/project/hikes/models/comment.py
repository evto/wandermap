from django.conf import settings
from django.db import models


class Comment(models.Model):

    user = models.ForeignKey(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        related_name='comments',
        on_delete=models.SET_NULL,
        null=True,
    )

    review = models.ForeignKey(
        verbose_name='review',
        to='hikes.Review',
        related_name='comments',
        on_delete=models.CASCADE,
    )

    content = models.TextField(
        verbose_name='review_comment',
    )

    created = models.DateTimeField(
        verbose_name='date_created',
        auto_now_add=True,
    )

    modified = models.DateTimeField(
        verbose_name='date_modified',
        auto_now=True,
    )

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'
        ordering = ['-modified']
