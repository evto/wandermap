import random

from django.conf import settings
from django.db import models


def code_generator(length=6):
    numbers = '0123456789'
    return ''.join(random.choice(numbers) for i in range(length))


class Profile(models.Model):
    user = models.OneToOneField(
        verbose_name='user',
        to=settings.AUTH_USER_MODEL,
        related_name='profile',
        on_delete=models.CASCADE,
    )

    location = models.CharField(
        verbose_name='location',
        max_length=30,
    )

    status = models.CharField(
        verbose_name='status',
        max_length=50,
        blank=True,
    )

    things_love = models.TextField(
        verbose_name='things_user_love',
    )

    description = models.TextField(
        verbose_name='user_description',
    )

    image = models.ImageField(
        blank=True
    )

    registration_code = models.CharField(
        verbose_name='registration_code',
        max_length=15,
        default=code_generator,
    )
