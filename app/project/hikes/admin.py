from django.contrib import admin

from project.hikes.models import Hike, Review, Comment, Profile, ReviewLike, CommentLike, Category, ReviewDislike
from project.hikes.models.hike import HikeImage, FavoriteHike, BookmarkedHike


class HikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'name', 'starting_point', 'destination', 'hiking_time', 'length', 'highest_point',
                    'webcams', 'fitness_level', 'technique', 'season', 'category']


class HikeImageAdmin(admin.ModelAdmin):
    list_display = ['image']


class ReviewAdmin(admin.ModelAdmin):
    list_display = ['user', 'hike', 'content', 'rating']


class CommentAdmin(admin.ModelAdmin):
    list_display = ['user', 'review', 'content']


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'location', 'status', 'things_love', 'description']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']


class ReviewLikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'review']


class ReviewDislikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'review']


class CommentLikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'comment']


class FavoriteHikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'hike']


class BookmarkedHikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'hike']


admin.site.register(Hike, HikeAdmin)
admin.site.register(HikeImage, HikeImageAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ReviewLike, ReviewLikeAdmin)
admin.site.register(ReviewDislike, ReviewDislikeAdmin)
admin.site.register(CommentLike, CommentLikeAdmin)
admin.site.register(FavoriteHike, FavoriteHikeAdmin)
admin.site.register(BookmarkedHike, BookmarkedHikeAdmin)
