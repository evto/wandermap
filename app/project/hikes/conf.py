from django.apps import AppConfig


class HikesApp(AppConfig):
    name = 'project.hikes'
    verbose_name = "wandermap"

    def ready(self):
        from . import signals  # noqa
