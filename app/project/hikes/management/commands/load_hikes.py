from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
import pandas as pd
from geopy.geocoders import Nominatim

from project.hikes.models import Hike


class Command(BaseCommand):

    def handle(self, *args, **options):
        hikes_db = pd.read_csv('/app/data/hikes_db.csv')
        hikes_db = hikes_db.where((pd.notnull(hikes_db)), None)
        geolocator = Nominatim()
        for index, row in hikes_db.iterrows():
            start = geolocator.geocode(row['starting_point'])
            destination = geolocator.geocode(row['destination'])
            if start and destination:

                hike, created = Hike.objects.get_or_create(
                    user=User.objects.get(username=row['user']),
                    name=row['name'],
                    starting_point=[start.latitude, start.longitude],
                    destination=[destination.latitude, destination.longitude],
                    route=f"{row['starting_point']} - {row['destination']}",
                    hiking_time=row['hiking_time'],
                    height_gain=row['height_gain'],
                    height_loss=row['height_loss'],
                    length=row['length'],
                    highest_point=row['highest_point'],
                    # webcams=row['webcams'],
                    fitness_level=row['fitness_level'],
                    technique=row['technique'],
                    season=row['season'],
                    # category=None
                )
